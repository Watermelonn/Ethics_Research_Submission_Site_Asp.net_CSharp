﻿<%@ Page Title="FSE REC Submissions Website" Language="C#" MasterPageFile="~/MasterPageNoPageArea.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FSEREC.Default" %>

 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Stylesheet/Default.css" rel="stylesheet" />

    <style type="text/css">
        .title {
            text-align: center;
            margin-top: 60px;
        }

        .subHeading {
            color: #979797;
            font-size: 30px;
            text-align: center;
            padding: 0;
            margin: 0;
            margin-top: 10px;
        }

        h2 {
            padding: 0;
            margin: 0;
            margin-top: 60px;
            margin-bottom: 20px;
        }

        u {
            text-decoration: none;
            border-bottom: 3px solid #161616;
            padding-bottom: 2px;
        }

        .bannerImage {
            width: 100%;
        }

        .submitApplication {
            width: 100%;
            max-width: 1500px;
        }

        .HowtoSubmitWrapper {
            overflow: hidden;
            display: inline-block;
        }

        .heading {
            padding-left: 2vw;
            padding-right: 2vw;
        }

        .textArea {
            
            padding-left: 2vw;
            padding-right: 2vw;
        }

        .bottom {
            margin-bottom: 5vh;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <img src="Images/thornton-blur-text.jpg" class="bannerImage" />
    <div class="textArea">

    <h1 class="title">Hurry! The deadline for final submissions is <asp:Label ID="lblDeadline" runat="server" Text=""></asp:Label></h1>
    <h2 class="subHeading">All applications will be considered by the Research Ethics Committee on <asp:Label ID="lblMeetingDate" runat="server" Text=""></asp:Label></h2>

    </div>


    <h2 class="heading">New Applications</h2>


    <div class="textArea">

        <p>To start a new application, click <a href="/student/newSubmission.aspx">here</a> and follow the details on the page to submit the required documents.</p>

        <p>Please submit your applications within sufficient time if you are working against tight deadlines.</p>

        <p>Please obtain all necessary signatures. This is very important! Applications without the required signatures will be returned to the applicant.</p>
    </div>


    <h2 class="heading">Contact Us</h2>

    <div class="textArea">
        <p>Got any problems, questions or queries? Feel free to contact Pauline Morton at: <a href="mailto:THORNTON-REC@chester.ac.uk">THORNTON-REC@chester.ac.uk</a></p>

    </div>


    <h2 class="heading">Guide to Submit an Application</h2>

    <div class="textArea bottom">
        <div class="HowtoSubmitWrapper">
            <img class="submitApplication" alt="How to submit an application" src="Images/SubmitApplication.png" />
        </div>
    </div>


</asp:Content>
