﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FSEREC.Committee
{

    public partial class CommitteeShare : System.Web.UI.Page
    {

        int levelNumber;
        string firstValue;
        string secondValue;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void dgvCommittee_DataBound(object sender, EventArgs e)
        {
            

            foreach (GridViewRow row in dgvCommittee.Rows)
            {

                Label lblFile = (Label)row.FindControl("lblFile");
                LinkButton btnDownload = (LinkButton)row.FindControl("btnDownload");

                if (lblFile.Text == "Attached file: ")
                {

                    btnDownload.Visible = false;
                    lblFile.Visible = false;

                }

                Label lblSortKey = (Label)row.FindControl("lblSortKey");

                string[] levels = lblSortKey.Text.Split('.');

                if (levels[1] != "null")
                {
                    row.Cells[0].Style.Add("padding-left", "150px");
                }
                if (levels[2] != "null")
                {
                    row.Cells[0].Style.Add("padding-left", "300px");
                    LinkButton btnReply = (LinkButton)row.FindControl("btnReply");

                    btnReply.Visible = false;


                }

                Label lblUserID = (Label)row.FindControl("lblUserID");

                string userId = User.Identity.GetUserId();
                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new IdentityDbContext("NewerDatabase")));

                if (userManager.IsInRole(userId, "Admin"))
                {
                    LinkButton btnDelete = (LinkButton)row.FindControl("btnDelete");
                    btnDelete.Visible = true;
                }

                if (lblUserID.Text == HttpContext.Current.User.Identity.GetUserId())
                {
                    Label lblName = (Label)row.FindControl("lblName");
                    LinkButton btnDelete = (LinkButton)row.FindControl("btnDelete");

                    btnDelete.Visible = true;
                    lblName.ForeColor = System.Drawing.Color.DodgerBlue;

                }

            }

            int numberOfRows = dgvCommittee.Rows.Count;

            if (numberOfRows > 10)
            {
                for (int a = 10; a < numberOfRows; a = a + 1)
                {
                    Label lblSortKey = (Label)dgvCommittee.Rows[a].FindControl("lblSortKey");

                    string[] levels = lblSortKey.Text.Split('.');

                    if (levels[1] == "null" && levels[2] == "null")
                    {

                        dgvCommittee.PageSize = a;
                        break;

                    }

                }
            }


        }
        protected void btnReply_Click(object sender, EventArgs e)
        {


            LinkButton btnReply = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnReply.NamingContainer;

            LinkButton btnCancel = (LinkButton)row.FindControl("btnCancel");
            TextBox txtComment = (TextBox)row.FindControl("txtComment");
            LinkButton btnDelete = (LinkButton)row.FindControl("btnDelete");
            LinkButton btnComment = (LinkButton)row.FindControl("btnComment");

            btnReply.Visible = false;
            btnDelete.Visible = false;
            btnCancel.Visible = true;
            txtComment.Visible = true;
            btnComment.Visible = true;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LinkButton btnCancel = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnCancel.NamingContainer;

            LinkButton btnReply = (LinkButton)row.FindControl("btnReply");
            TextBox txtComment = (TextBox)row.FindControl("txtComment");
            LinkButton btnDelete = (LinkButton)row.FindControl("btnDelete");
            LinkButton btnComment = (LinkButton)row.FindControl("btnComment");

            btnReply.Visible = true;
            btnDelete.Visible = true;
            btnCancel.Visible = false;
            txtComment.Visible = false;
            btnComment.Visible = false;

        }

        protected void btnComment_Click(object sender, EventArgs e)
        {
            LinkButton btnComment = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnComment.NamingContainer;

            Label lblSortKey = (Label)row.FindControl("lblSortKey");

            string[] levels = lblSortKey.Text.Split('.');

            if (levels[1] == "null")
            {

                firstValue = levels[0];

                levelNumber = 1;
                addComment(row);
            }
            else if (levels[2] == "null")
            {
                secondValue = levels[1];
                firstValue = levels[0];

                levelNumber = 2;
                addComment(row);
            }


        }

        protected void btnNewComment_Click(object sender, EventArgs e)
        {

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];

            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO tblCommitteeShare (committee_Comment, committee_Time, user_ID) OUTPUT INSERTED.committee_ID VALUES (@committee_Comment, @committee_Time, @user_ID)");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.Parameters.AddWithValue("@committee_Comment", txtNewComment.Text);
                cmd.Parameters.AddWithValue("@committee_Time", DateTime.Now);
                cmd.Parameters.AddWithValue("@user_ID", HttpContext.Current.User.Identity.GetUserId());

                Int32 committee_ID = (Int32)cmd.ExecuteScalar();

                string sortKey = "";

                sortKey = committee_ID + ".null.null";

                cmd.CommandText = "UPDATE tblCommitteeShare SET sortKey = @sortKey WHERE committee_ID = " + committee_ID;

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sortKey", sortKey);

                cmd.ExecuteNonQuery();

                if (uplNewComment.FileName != "")
                {
                    string current = DateTime.Now.ToString("yyyyMMddHHmmss");
                    string path = Server.MapPath("~/committeeFiles/");


                    path = path + current + "/";
                    Directory.CreateDirectory(path);

                    string saveLocation = ("/committeeFiles/" + current + "/" + uplNewComment.FileName);
                    string fileExtension = System.IO.Path.GetExtension(uplNewComment.FileName);
                    File.Delete(Server.MapPath(saveLocation));



                    cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";


                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@file_Name", uplNewComment.FileName);
                    cmd.Parameters.AddWithValue("@file_Size", uplNewComment.PostedFile.ContentLength);
                    cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                    cmd.Parameters.AddWithValue("@file_Location", saveLocation);
                    cmd.Parameters.AddWithValue("@file_No", 5);

                    Int32 file_ID = (Int32)cmd.ExecuteScalar();

                    uplNewComment.SaveAs(Server.MapPath(saveLocation));

                    cmd.CommandText = "UPDATE tblCommitteeShare SET file_ID = @file_ID WHERE committee_ID = " + committee_ID;

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@file_ID", file_ID);

                    cmd.ExecuteNonQuery();
                }
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void addComment(GridViewRow row)
        {
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];

            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO tblCommitteeShare (committee_Comment, committee_Time, user_ID) OUTPUT INSERTED.committee_ID VALUES (@committee_Comment, @committee_Time, @user_ID)");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                TextBox txtComment = (TextBox)row.FindControl("txtComment");

                cmd.Parameters.AddWithValue("@committee_Comment", txtComment.Text);
                cmd.Parameters.AddWithValue("@committee_Time", DateTime.Now);
                cmd.Parameters.AddWithValue("@user_ID", HttpContext.Current.User.Identity.GetUserId());

                Int32 committee_ID = (Int32)cmd.ExecuteScalar();

                string sortKey = "";

                if (levelNumber == 1)
                {

                    sortKey = firstValue + "." + committee_ID.ToString() + ".null";
                }
                else if (levelNumber == 2)
                {

                    sortKey = firstValue + "." + secondValue + "." + committee_ID.ToString();
                }

                cmd.CommandText = "UPDATE tblCommitteeShare SET sortKey = @sortKey WHERE committee_ID = " + committee_ID;

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sortKey", sortKey);

                cmd.ExecuteNonQuery();

            }
            Response.Redirect(Request.RawUrl);
        }

        protected void btnAttach_Click(object sender, EventArgs e)
        {
            uplNewComment.Visible = true;
            btnAttach.Visible = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            LinkButton btnDelete = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnDelete.NamingContainer;

            Label lblFileLocation = (Label)row.FindControl("lblFileLocation");
            Label lblCommitteeID = (Label)row.FindControl("lblCommitteeID");

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.CommandText = "DELETE tblCommitteeShare WHERE sortKey LIKE '%" + lblCommitteeID.Text + "%'";

                cmd.ExecuteNonQuery();
                if (File.Exists(Server.MapPath(lblFileLocation.Text)))
                {
                    File.Delete(Server.MapPath(lblFileLocation.Text));
                }


                Sqlconnect.Close();

            }


            Response.Redirect(Request.RawUrl);
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            LinkButton btnDownload = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnDownload.NamingContainer;

            Label lblFileLocation = (Label)row.FindControl("lblFileLocation");
            Label lblFileName = (Label)row.FindControl("lblFileName");
            Label lblFileType = (Label)row.FindControl("lblFileType");
            Label lblFileSize = (Label)row.FindControl("lblFileSize");

            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + lblFileName.Text + "\"");
            Response.AddHeader("Content-Length", lblFileSize.Text);
            Response.ContentType = "application/" + lblFileType.Text;
            Response.Flush();
            Response.TransmitFile(Server.MapPath(lblFileLocation.Text));
            Response.End();

        }
    }
}
