﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FSEREC.Committee
{
    public partial class viewSubmissions : System.Web.UI.Page
    {
        string cmd;
        string defaultcmd;

        protected void Page_Load(object sender, EventArgs e)
        {
            defaultcmd = repDataSource.SelectCommand;
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string userId = User.Identity.GetUserId();
                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new IdentityDbContext("NewerDatabase")));

                if (userId != null)
                {
                    if (userManager.IsInRole(userId, "Admin") | userManager.IsInRole(userId, "Committee"))
                    {
                        Page.Title = "User Submissions - FSE REC Submissions";

                    }
                    else if (userManager.IsInRole(userId, "User"))
                    {

                        Page.Title = "My Submissions - FSE REC Submissions";

                        defaultcmd = defaultcmd + " WHERE AspNetUsers.Id= '" + userId + "'";

                        repDataSource.SelectCommand = defaultcmd;

                        repDataSource.DataBind();
                        repSubmissions.DataBind();

                        if (repSubmissions.Items.Count == 0)
                        {
                            lblNoSubmissions.Visible = true;
                            drpFilter.Visible = false;
                            drpSort.Visible = false;
                            lblFilter.Visible = false;
                            lblSort.Visible = false;
                        }
                        
                    }
                }



                drpSort_SelectedIndexChanged(sender, e);

            }
        }

        protected void drpFilter_SelectedIndexChanged(object sender, EventArgs e)
        {

            cmd = defaultcmd;

            if (drpFilter.SelectedValue == "Open")
            {
                if (repDataSource.SelectCommand.Contains("WHERE"))
                {
                    cmd = defaultcmd + " AND tblSubmission.submission_Status = 'Open'";
                }
                else
                {
                    cmd = defaultcmd + " WHERE tblSubmission.submission_Status = 'Open'";
                }

                repDataSource.SelectCommand = cmd;
                repDataSource.DataBind();
                repSubmissions.DataBind();
            }
            else if (drpFilter.SelectedValue == "Closed")
            {
                if (repDataSource.SelectCommand.Contains("WHERE"))
                {
                    cmd = defaultcmd + " AND tblSubmission.submission_Status = 'Closed'";
                }
                else
                {
                    cmd = defaultcmd + " WHERE tblSubmission.submission_Status = 'Closed'";
                }

                repDataSource.SelectCommand = cmd;
                repDataSource.DataBind();
                repSubmissions.DataBind();
            }

            else if (drpFilter.SelectedValue == "Show All")
            {
                repDataSource.SelectCommand = cmd;
                repDataSource.DataBind();
                repSubmissions.DataBind();
            }

        }



        protected void drpSort_SelectedIndexChanged(object sender, EventArgs e)
        {

            drpFilter_SelectedIndexChanged(sender, e);

            if (drpSort.SelectedValue == "Submit Date: (Oldest to Newest)")
            {
                repDataSource.SelectCommand = cmd + " ORDER BY tblSubmission.submission_SubmitDate ASC";
                repDataSource.DataBind();
                repSubmissions.DataBind();
            }
            else if (drpSort.SelectedValue == "Submit Date: (Newest to Oldest)")
            {
                repDataSource.SelectCommand = cmd + " ORDER BY tblSubmission.submission_SubmitDate DESC";
                repDataSource.DataBind();
                repSubmissions.DataBind();
            }

        }

        protected void repSubmissions_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {


        }

        protected void repSubmissions_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            
            
        }
    }
}