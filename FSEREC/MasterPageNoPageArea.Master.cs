﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FSEREC
{
    public partial class MasterPageNoPageArea : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {

                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new IdentityDbContext("NewerDatabase")));
                var user = userManager.FindByName(Context.User.Identity.GetUserName());
                string userId = Context.User.Identity.GetUserId();

                try
                {
                    if (userManager.IsInRole(userId, "Admin"))
                    {
                        btnUserSubmissions.Visible = true;
                        btnAdmin.Visible = true;
                        btnAdmin.PostBackUrl = "/admin/admin.aspx";
                        btnCommittee.Visible = true;
                        btnCommittee.PostBackUrl = "/Committee/CommitteeShare.aspx";

                        btnUserSubmissions.PostBackUrl = "/committee/viewSubmissions.aspx";
                    }
                    else if (userManager.IsInRole(userId, "Committee"))
                    {

                        btnUserSubmissions.Visible = true;
                        btnCommittee.Visible = true;
                        btnCommittee.PostBackUrl = "/Committee/CommitteeShare.aspx";

                        btnUserSubmissions.PostBackUrl = "/committee/viewSubmissions.aspx";

                    }
                    else if (userManager.IsInRole(userId, "User"))
                    {
                        btnUserSubmissions.Text = "My Submissions";
                        btnUserSubmissions.ForeColor = System.Drawing.Color.IndianRed;
                        btnUserSubmissions.Visible = true;
                        btnUserSubmissions.PostBackUrl = "/committee/viewSubmissions.aspx";
                    }


                    string firstname = "";
                    string surname = "";

                    var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
                    using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
                    {

                        Sqlconnect.Open();
                        SqlCommand cmd = new SqlCommand("SELECT Forename, Surname FROM AspNetUsers WHERE Id = '" + userId + "'");
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = Sqlconnect;

                        SqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            firstname = dr["Forename"].ToString();
                            surname = dr["Surname"].ToString();
                        }
                        Sqlconnect.Close();
                    }

                    string name = firstname + " " + surname;


                    string username = Context.User.Identity.GetUserName();

                    lblSignOut.Text = name;



                    if (username.Contains("@"))
                    {
                        btnSignOut.Text = " (Sign Out)";
                        btnSignOut.Enabled = true;
                        btnLogin.Text = "Account";
                        btnLogin.PostBackUrl = "/student/ManagePassword.aspx";
                    }
                    else
                    {
                        lblSignOut.Text = name + " " + username;
                        btnLogin.Text = "Sign Out";
                    }
                }



                catch
                {
                    HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    btnSignOut.Enabled = false;
                    btnSignOut.Text = "";
                    btnLogin.Text = "Login";
                    btnLogin.PostBackUrl = "/login.aspx";
                }

            }


            else
            {


                btnSignOut.Enabled = false;
                btnSignOut.Text = "";
                btnLogin.Text = "Login";
                btnLogin.PostBackUrl = "/login.aspx";

            }


        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Response.Redirect("/signOut.aspx?signout=true");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (btnLogin.Text == "Sign Out")
            {
                HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                Response.Redirect("/signOut.aspx?signout=true");
            }
        }

        protected void s_Click(object sender, EventArgs e)
        {

                Response.Redirect("/signOut.aspx?signout=secret");
            
        }

    }
}