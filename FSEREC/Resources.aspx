﻿<%@ Page Title="Resources - FSE REC Submissions" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Resources.aspx.cs" Inherits="FSEREC.Resources" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Stylesheet/Resources.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class ="mainContentWrapper">
        <h1 class="Title">
            Useful Files
        </h1>
        <div class="line">
        </div>
    <p>Below is a list of useful files to support your application:</p>

    <h2>Information And Guidance For Application And Superior:</h2>
    <ul>
        <li class ="links"><a href="%2Fdownloads%2FFSE-REC%20Guidance%20notes%20for%20applicants%20and%20supervisors.pdf">FSE-REC Guidance notes for applicants and supervisors.pdf</a></li>
        <li class ="links"><a href="%2Fdownloads%2FResearch%20Governance%20Handbook%202.8.pdf">University of Chester Research Governance Handbook</a></li>
        <li class ="links"><a href="%2Fdownloads%2FResearchEthicsFlowchart.pdf">Research Ethics Flowchart</a></li>
        <li class ="links"><a href="%2Fdownloads%2FCode%20of%20practice%20for%20research%20-%20UKRIO.pdf">Code Of Practice For Research: Promoting Good Practice And Preventing Misconduct​</a></li>
        <li class ="links"><a href="%2Fdownloads%2FFSE-REC%20Research%20outside%20England.docx">Research Outside England</a></li>
        <li class ="links"><a href="%2Fdownloads%2FFSE-%20REC%20Umbrella.docx">Umbrella Projects</a></li>
    </ul>

    <h2>Application Forms And Templates For Supporting Documentation:</h2>
    <ul>
        <li class ="links"><a href="%2Fdownloads%2FApplication%20form%20Nov%202016.docx">Application form Nov 2016</a></li>
        <li class ="links"><a href="%2Fdownloads%2FFSE-REC%20Specimen%20Participant%20Information%20Sheet.doc">Specimen Participant Information Sheet (PIS)</a></li>
        <li class ="links"><a href="%2Fdownloads%2FFSE-REC%20Specimen%20consent%20form.doc">Specimen Consent Form</a></li>
        <li class ="links"><a href="%2Fdownloads%2FFSE-REC%20UOC%20risk%20assessment%20pro-forma.docx">University Of Chester Risk Assessment Pro-Forma </a></li>
    </ul>

    <h2>Forms/Information For Committee Members:</h2>
    <ul>
        <li class ="links"><a href="%2Fdownloads%2FFSE-REC%20Lead%20Reviewer%27s%20Assessment%20Checklist.doc">Lead Reviewer's Assessment Checklist</a></li>
        <li class ="links"><a href="%2Fdownloads%2FFSE-REC%20Lead%20Reviewer%27s%20Assessment%20Checklist%20Retrospective.doc">Lead Reviewer's Assessment Checklist Retrospective</a></li>
    </ul>

    <h2>Human Tissues And Cell Lines</h2>
    <ul>
        <li class ="links"><a href="%2Fdownloads%2FHELA.pdf">HELA</a></li>
        <li class ="links"><a href="%2Fdownloads%2Fbjc2014166a.pdf">Guidelines For The Use Of Cell Lines In Biomedical Research</a></li>
        <li class ="links"><a href="https%3A%2F%2Fwww.hta.gov.uk%2Ffaqs%2Fresearch-faqs​">Human Tissue Authority - FAQS</a></li>
    </ul>

    <h2>NHS Research Governance</h2>
    <p>
        If a research project is to include NHS patients or service users it needs to receive approval 
        from an NHS Research Ethics Committee. This will involve completing and submitting an IRAS form 
        which is an electronic application system. See <a href="/downloads/NHS ethics - quick guide v3 May2012.pdf">this document</a> for more information. These projects 
        will also require sponsorship​. Where the University agrees to act as Sponsor, the nominated representative 
        is Marie-Anne O'Neil who will check and authorise IRAS applications accordingly. 
    </p>
    <ul>
        <li class ="links"><a href="%2Fdownloads%2Fdoes%20my%20project%20require%20ethicial%20review.pdf">Does My Project Require Ethical Review? </a></li>
        <li class ="links"><a href="%2Fdownloads%2FNHS%20ethics%20-%20quick%20guide%20v3%20May2012.pdf">Quick Guide To NHS Ethics</a></li>
        <li class ="links"><a href="%2Fdownloads%2FSponsorship_procedure_V1.8.pdf">Sponsorship Procedure</a></li>
        <li class ="links"><a href="http%3A%2F%2Fwww.hra-decisiontools.org.uk%2Fethics%2F">Health Research Authority Decision Tool (Useful Link)</a></li>
        <li class ="links"><a href="http%3A%2F%2Fwww.hra.nhs.uk%2Fnews%2Frec%2F">Health Research Authority News (Useful Link)</a></li>
    </ul>
    </div>
</asp:Content>
