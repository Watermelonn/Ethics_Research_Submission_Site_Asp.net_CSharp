﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace FSEREC.admin
{
    public partial class admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void dgvAdmin_DataBound(object sender, EventArgs e)
        {
            string Id = "";
            string role = "";



            foreach (GridViewRow row in dgvAdmin.Rows)
            {

                System.Web.UI.WebControls.Button btnDelete = (System.Web.UI.WebControls.Button)row.FindControl("btnDelete");

                Id = row.Cells[0].Text;

                string userId = Context.User.Identity.GetUserId();

                if (Id == userId)
                {
                    btnDelete.Visible = false;
                }

                var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
                using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
                {

                    Sqlconnect.Open();
                    SqlCommand cmd = new SqlCommand("SELECT AspNetUsers.Id, AspNetRoles.Name FROM AspNetRoles INNER JOIN AspNetUserRoles ON AspNetRoles.Id = AspNetUserRoles.RoleId INNER JOIN AspNetUsers ON AspNetUserRoles.UserId = AspNetUsers.Id WHERE AspNetUsers.Id = '" + Id + "'");
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = Sqlconnect;

                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        role = dr["Name"].ToString();
                    }
                    Sqlconnect.Close();
                }

                DropDownList drp = (DropDownList)row.FindControl("drpRoles");

                if (role == "Admin")
                {
                    drp.SelectedIndex = 2;

                }
                else if (role == "Committee")
                {
                    drp.SelectedIndex = 1;
                }
                else if (role == "User")
                {
                    drp.SelectedIndex = 0;
                }
            }

            var connect = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connect.ConnectionString))
            {
                DateTime submissionDate;
                DateTime meetingDate;



                try
                {
                    Sqlconnect.Open();
                    SqlCommand cmd = new SqlCommand("SELECT MAX(deadline_Date) FROM tblMeetingDates");

                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = Sqlconnect;
                    submissionDate = ((DateTime)cmd.ExecuteScalar());

                    calendarDeadlineDate.SelectedDate = submissionDate;
                    lblDeadlineDate.Text = "Current date: " + calendarDeadlineDate.SelectedDate.ToString("dddd d MMMM yyyy");

                    cmd.CommandText = "SELECT MAX(meeting_Date) FROM tblMeetingDates";

                    meetingDate = ((DateTime)cmd.ExecuteScalar());

                    calendarMeetingDate.SelectedDate = meetingDate;
                    lblSelectedDate.Text = "Current date: " + calendarMeetingDate.SelectedDate.ToString("dddd d MMMM yyyy");

                }
                catch
                {

                }

                Sqlconnect.Close();
            }


        }




        protected void drpRoles_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.NamingContainer;

            string id = row.Cells[0].Text;

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("UPDATE AspNetUserRoles SET RoleId = @RoleId WHERE UserId = '" + id + "'");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;


                string role = "";

                if (ddl.SelectedValue == "0")
                {
                    role = "1";
                }
                else if (ddl.SelectedValue == "1")
                {
                    role = "2";
                }
                else if (ddl.SelectedValue == "2")
                {
                    role = "3";
                }


                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@RoleId", role);

                cmd.ExecuteNonQuery();

                Sqlconnect.Close();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            dgvAdmin.AllowSorting = false;

            string cmd = SqlDataSource1.SelectCommand;

            cmd = cmd + " WHERE AspNetUsers.Forename LIKE '%" + txtSearch.Text + "%' OR AspNetUsers.Forename LIKE '%" + txtSearch.Text + "%' OR AspNetUsers.Surname LIKE '%" + txtSearch.Text + "%' OR AspNetUsers.Institution LIKE '%" + txtSearch.Text + "%' OR AspNetUsers.Email LIKE '%" + txtSearch.Text + "%'";

            SqlDataSource1.SelectCommand = cmd;
            dgvAdmin.DataBind();

            btnResults.Visible = true;
            lblResults.Visible = true;

            btnSearch.Visible = false;
            txtSearch.Visible = false;
            lblSearch.Visible = false;

            lblResults.Text = "Results for '" + txtSearch.Text + "':";

        }

        protected void btnResults_Click(object sender, EventArgs e)
        {

            Response.Redirect(Request.RawUrl);

        }
        protected void calendarMeetingDate_SelectionChanged(object sender, EventArgs e)
        {
            lblSelectedDate.Text = "Selected Date: " + calendarMeetingDate.SelectedDate.ToString("dddd d MMMM yyyy");
        }

        protected void calendarDeadlineDate_SelectionChanged(object sender, EventArgs e)
        {
            lblDeadlineDate.Text = "Selected Date: " + calendarDeadlineDate.SelectedDate.ToString("dddd d MMMM yyyy");
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("DELETE FROM tblMeetingDates");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO tblMeetingDates(meeting_Date, deadline_Date)VALUES(@meeting, @deadline)";

                cmd.Parameters.AddWithValue("@meeting", calendarMeetingDate.SelectedDate);
                cmd.Parameters.AddWithValue("@deadline", calendarDeadlineDate.SelectedDate);

                cmd.ExecuteNonQuery();

                Response.Redirect(Request.RawUrl);

                Sqlconnect.Close();
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            System.Web.UI.WebControls.Button btnDelete = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btnDelete.NamingContainer;

            string id = row.Cells[0].Text;

            List<string> submission_ID = new List<string>();
            List<string> file_ID = new List<string>();
            List<string> comment_ID = new List<string>();
            List<string> comment_sortKey = new List<string>();
            List<string> committee_sortKey = new List<string>();

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("SELECT submission_ID FROM tblUser_Submission WHERE Id = '" + id + "'");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;
                SqlDataReader dr = cmd.ExecuteReader();
                try
                {


                    while (dr.Read())
                    {
                        submission_ID.Add(dr["submission_ID"].ToString());
                    }
                    dr.Close();
                }
                catch
                {

                }

                cmd.CommandText = "DELETE tblUser_Submission WHERE Id = '" + id + "'";

                cmd.ExecuteNonQuery();




                foreach (string ID in submission_ID)
                {
                    cmd.CommandText = "SELECT file_ID FROM tblSubmission_File WHERE Submission_ID = " + ID;


                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            file_ID.Add(dr["file_ID"].ToString());
                        }

                        dr.Close();
                    }
                    catch
                    {

                    }

                    cmd.CommandText = "DELETE tblSubmission_File WHERE Submission_ID = " + ID;

                    cmd.ExecuteNonQuery();

                }

                cmd.CommandText = "SELECT comment_ID FROM tblComments WHERE user_ID = '" + id + "'";

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                        comment_ID.Add(dr["comment_ID"].ToString());
                        }

                        dr.Close();
                    }
                    catch
                    {

                    }


                
                
                cmd.CommandText = "SELECT sortKey FROM tblCommitteeShare WHERE user_ID = '" + id + "'";


                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    committee_sortKey.Add(dr["sortKey"].ToString());

                }

                dr.Close();

                foreach (string ID in committee_sortKey)
                {
                    cmd.CommandText = "DELETE tblCommitteeShare WHERE sortKey LIKE '" + ID + "'";

                    cmd.ExecuteNonQuery();
                }
                

                foreach (string ID in file_ID)
                {



                    cmd.CommandText = "DELETE tblFile WHERE file_ID = " + ID;

                    cmd.ExecuteNonQuery();
                }

                foreach (string ID in comment_ID)
                {


                   
                    
                    cmd.CommandText = "SELECT sortKey FROM tblComments WHERE comment_ID = " + ID;

                 
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {

                            comment_sortKey.Add(dr["sortKey"].ToString());
                            
                        }

                        dr.Close();
                 

                }

                


                foreach (string ID in submission_ID)
                {
                    cmd.CommandText = "DELETE tblSubmission_Comments WHERE submission_ID = " + ID;

                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "DELETE tblSubmission WHERE Submission_ID = " + ID;

                    cmd.ExecuteNonQuery();
                }

                foreach (string ID in comment_sortKey)
                {
                    cmd.CommandText = "DELETE tblComments WHERE sortKey LIKE '" + ID + "'";

                    cmd.ExecuteNonQuery();
                }

                cmd.CommandText = "DELETE AspNetUserRoles WHERE userId = '" + id + "'";

                cmd.ExecuteNonQuery();

                cmd.CommandText = "DELETE AspNetUsers WHERE Id = '" + id + "'";

                cmd.ExecuteNonQuery();


                Response.Redirect(Request.RawUrl);

            }
        }

    }
}


