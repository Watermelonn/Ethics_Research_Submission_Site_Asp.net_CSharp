﻿<%@ Page Title="My Submission - FSE REC Submissions" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="submissionDetail.aspx.cs" Inherits="FSEREC.student.submissonDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .btnEdit {
            margin-left: 10px;
            margin-bottom: 10px;
        }

        .emailAddress {
            margin-top: 0;
            display: block;
        }

        .submissions {
            margin-bottom: 20px;
            margin-top: 20px;
        }

        .link {
            padding-left: 5px;
            padding-right: 5px;
        }

        p {
            font-size: 26px;
            text-wrap: none;
            margin: 0;
        }

        .section {
            display: block;
            margin-bottom: 10px;
        }

        .bottom {
            margin-bottom: 30px;
        }

        .largeButtons {
            margin-right: 5px;
            cursor: pointer;
        }


        .commentsHeader {
            margin-top: 30px;
            display: block;
            margin-bottom: 20px;
        }

        .line {
            margin-bottom: 10px;
        }

        .status {
            padding-left: 10px;
        }

        .above {
            padding-top: 30px;
        }

        .comments {
            display: block;
            margin-top: 12px;
        }

        .gridview {
            margin-top: 12px;
            margin-bottom: 10px;
        }

        .commentItem {
            display: block;
        }

        .comment {
            margin-top: 10px;
            margin-bottom: 20px;
        }

        .commentContainer {
            background-color: #FFFFFF;
            border: solid 1px #cecece;
            padding: 10px;
        }

        .lineBreak {
            height: 1px;
            background-color: #d0d0d0;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .miniLineBreak {
            height: 1px;
            background-color: #d0d0d0;
            display: block;
            margin-top: 10px;
        }

        .time {
            float: right;
        }

        .reply {
            margin-bottom: 10px;
            margin-right: 10px;
        }

            .reply :last-child {
                margin-bottom: 0px;
            }

        .newComment {
            display: block;
            margin-top: 10px;
        }

        .add {
            margin-right: 10px;
        }

        .validation {
            margin-top: 8px;
            margin-bottom: 8px;
            display: block;
        }

        .emailLink {
            font-size: 26px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:FormView ID="submissionDetail" runat="server" DataSourceID="formViewDataSource">
        <ItemTemplate>
            <asp:Label ID="lblId" runat="server" Text='<%#Eval("Id")%>' Visible="False"></asp:Label>
            <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("submission_Title") %>' Font-Size="50px" ForeColor="#333" Font-Names="Lato-Regular" CssClass="title"></asp:Label>
            <asp:TextBox ID="txtTitle" runat="server" ReadOnly="False" Font-Size="50px" Font-Names="Lato" MaxLength="50" TextMode="SingleLine" CssClass="title" Visible="False" Text='<%# Eval("submission_Title") %>' BackColor="Transparent" BorderStyle="Solid" BorderWidth="1px" BorderColor="#7C7C7C"></asp:TextBox>


            <asp:Button ID="btnEditTitle" runat="server" Text="Edit Title" Font-Names="Lato" Font-Size="22px" Visible="False" CssClass="smallButtons" OnClick="btnEditTitle_Click" />
            <asp:Button ID="btnSaveTitle" runat="server" Text="Save" Font-Names="Lato" Font-Size="22px" Visible="False" CssClass="smallButtons" OnClick="btnSaveTitle_Click" />
            <asp:Button ID="btnCancelTitle" runat="server" Text="Cancel" Font-Names="Lato" Font-Size="22px" Visible="False" CssClass="smallButtons" OnClick="btnCancelTitle_Click" />

            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("submission_Status") %>' Font-Size="35px" Font-Names="Lato" ForeColor="#333" CssClass="status"></asp:Label>

            <asp:Label ID="lblName" runat="server" Text='<%#Eval("Forename") + " " + Eval("Surname")%>' CssClass="emailAddress" Font-Size="26px" Font-Names="Lato"></asp:Label>
            <asp:Label ID="lblEmail" runat="server" Text='<%#"<a class=emailLink href=mailto:"+Eval("Email")+">"+Eval("Email")+"</a>" %>' CssClass="emailAddress" Font-Size="26px" Font-Names="Lato"></asp:Label>

            <div class="submissions">
                <p class="submitted">Submitted: <%# Eval("submission_SubmitDate") %></p>
                <p class="submitted">Last Edited: <%# Eval("submission_LastEdited") %></p>
            </div>

            <div class="section">
                <asp:Label ID="lblApplication" Font-Names="Lato" Font-Size="26px" runat="server" Text="Application Form: " CssClass="file"></asp:Label>
                <asp:Label ID="lblApplicationName" Font-Names="Lato" Font-Size="26px" runat="server" Text="" CssClass="file"></asp:Label>
                <asp:LinkButton ID="btnDownload1" OnClick="btnDownload1_Click" runat="server" Font-Names="Lato" Font-Size="26px" CssClass="link">Download</asp:LinkButton>
                <asp:FileUpload ID="uplApplication" runat="server" CssClass="upload" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" Visible="False" />
                <asp:Button ID="btnReplace1" runat="server" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnReplace1_Click" Visible="False" Text="Replace" />
                <asp:Button ID="btnSave1" runat="server" ValidationGroup="upload1" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnSave1_Click" Visible="False" Text="Save" />
                <asp:Button ID="btnCancel1" runat="server" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnCancel1_Click" Visible="False" Text="Cancel" />
                <asp:RegularExpressionValidator ID="valFormatApplication" ValidationGroup="upload1" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" Display="Dynamic" ErrorMessage="The application form must be uploaded in either a PDF or word file format." ControlToValidate="uplApplication" ValidationExpression="^.+(.doc|.docx|.pdf|.docm)$"></asp:RegularExpressionValidator>
            </div>

            <div class="section">
                <asp:Label ID="lblReferences" Font-Names="Lato" Font-Size="26px" runat="server" Text="List of References:" CssClass="file"></asp:Label>
                <asp:Label ID="lblReferencesName" Font-Names="Lato" Font-Size="26px" runat="server" Text="" CssClass="file"></asp:Label>
                <asp:LinkButton ID="btnDownload3" OnClick="btnDownload3_Click" Font-Names="Lato" Font-Size="26px" runat="server" CssClass="link">Download</asp:LinkButton>
                <asp:FileUpload ID="uplReferences" runat="server" CssClass="upload" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" Visible="False" />
                <asp:Button ID="btnReplace3" Visible="False" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnReplace3_Click" runat="server" Text="Replace" />
                <asp:Button ID="btnSave3" runat="server" ValidationGroup="upload3" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnSave3_Click" Visible="False" Text="Save" />
                <asp:Button ID="btnCancel3" runat="server" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnCancel3_Click" Visible="False" Text="Cancel" />
                <asp:RegularExpressionValidator ID="valFormatReferences" ValidationGroup="upload3" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" Display="Dynamic" ErrorMessage="References must be uploaded in either a PDF or word file format." ControlToValidate="uplReferences" ValidationExpression="^.+(.doc|.docx|.pdf|.docm)$"></asp:RegularExpressionValidator>

            </div>

            <div class="section">
                <asp:Label ID="lblCV" runat="server" Font-Names="Lato" Font-Size="26px" Text="Main Researcher C.V.:" CssClass="file"></asp:Label>
                <asp:Label ID="lblCVName" Font-Names="Lato" Font-Size="26px" runat="server" Text="" CssClass="file"></asp:Label>
                <asp:LinkButton ID="btnDownload2" OnClick="btnDownload2_Click" Font-Names="Lato" Font-Size="26px" runat="server" CssClass="link">Download</asp:LinkButton>
                <asp:FileUpload ID="uplCV" runat="server" CssClass="upload" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" Visible="False" />
                <asp:Button ID="btnReplace2" Visible="False" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnReplace2_Click" runat="server" Text="Replace" />
                <asp:Button ID="btnSave2" runat="server" ValidationGroup="upload2" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnSave2_Click" Visible="False" Text="Save" />
                <asp:Button ID="btnCancel2" runat="server" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnCancel2_Click" Visible="False" Text="Cancel" />
                <asp:RegularExpressionValidator ID="valFormatCV" ValidationGroup="upload2" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" Display="Dynamic" ErrorMessage="The C.V. must be uploaded in either a PDF or word file format." ControlToValidate="uplCV" ValidationExpression="^.+(.doc|.docx|.pdf|.docm)$"></asp:RegularExpressionValidator>
            </div>

            <div class="section bottom">
                <asp:Label ID="lblSupporting" Font-Names="Lato" Font-Size="26px" runat="server" Text="Supporting Files:" CssClass="file"></asp:Label>
                <asp:Label ID="lblSupportingName" Font-Names="Lato" Font-Size="26px" runat="server" Text="" CssClass="file"></asp:Label>
                <asp:LinkButton ID="btnDownload4" Font-Names="Lato" OnClick="btnDownload4_Click" Font-Size="26px" runat="server" CssClass="link">Download</asp:LinkButton>
                <asp:FileUpload ID="uplSupporting" runat="server" CssClass="upload" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" Visible="False" />
                <asp:Button ID="btnReplace4" Visible="False" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnReplace4_Click" runat="server" Text="Replace" />
                <asp:Button ID="btnSave4" runat="server" ValidationGroup="upload4" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnSave4_Click" Visible="False" Text="Save" />
                <asp:Button ID="btnCancel4" runat="server" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" OnClick="btnCancel4_Click" Visible="False" Text="Cancel" />
                <asp:Button ID="btnDelete" Visible="False" CssClass="smallButtons" Font-Names="Lato" Font-Size="22px" runat="server" Text="Delete" OnClick="btnDelete_Click" />

                <asp:RegularExpressionValidator ID="valFormatSupporting" ValidationGroup="upload4" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" Display="Dynamic" ErrorMessage="Supporting documents must be uploaded in a zip file." ControlToValidate="uplSupporting" ValidationExpression="^.+(.zip|.ZIP)$"></asp:RegularExpressionValidator>

            </div>

            <asp:Literal ID="ltrFileError" runat="server"></asp:Literal>

            <asp:Button ID="btnEdit" CssClass="largeButtons" BackColor="White" BorderColor="#7c7c7c" Font-Names="Lato" Font-Size="26px" Height="50px" Width="200px" BorderWidth="1px" OnClick="btnEdit_Click" runat="server" Text="Edit Submission" />
            <asp:Button ID="btnDownloadAll" CssClass="largeButtons" BackColor="White" BorderColor="#7c7c7c" Font-Names="Lato" Font-Size="26px" Height="50px" Width="200px" BorderWidth="1px" runat="server" Text="Download All" OnClick="btnDownloadAll_Click" />
            <asp:Button ID="btnSave" CssClass="largeButtons" Visible="False" OnClick="btnSave_Click" BackColor="White" BorderColor="#7c7c7c" Font-Names="Lato" Font-Size="26px" Height="50px" Width="200px" BorderWidth="1px" runat="server" Text="Finished" />
            <asp:Button ID="btnStatus" CssClass="largeButtons" runat="server" Text="Close Application" Visible="False" OnClick="btnStatus_Click" BackColor="White" BorderColor="#7c7c7c" Font-Names="Lato" Font-Size="26px" Height="50px" BorderWidth="1px" />

            <asp:Button ID="btnDeleteSubmission" CssClass="largeButtons" Visible="False" OnClientClick="return confirm('Are you sure you want to delete this submission?')" OnClick="btnDeleteSubmission_Click" BackColor="White" BorderColor="#7c7c7c" Font-Names="Lato" Font-Size="26px" Height="50px" BorderWidth="1px" runat="server" Text="Delete Submission" />

        </ItemTemplate>
    </asp:FormView>



    <asp:Label ID="lblComments" runat="server" CssClass="commentsHeader" Font-Names="Lato-Regular" ForeColor="indianred" Font-Size="35px" Text="Committee Comments" Visible="False"></asp:Label>


    <asp:TextBox ID="txtNewComment" Visible="false" ValidationGroup="newComment" runat="server" Width="70%" Height="70px" placeholder="Leave a comment" TextMode="MultiLine" MaxLength="200" Font-Size="24px" Font-Names="Lato" CssClass="newComment"></asp:TextBox>

    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" CssClass="validation" runat="server" ErrorMessage="Please enter a comment less than 200 characters." ControlToValidate="txtNewComment" ValidationExpression="^[\s\S]{0,200}$" ValidationGroup="newComment"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="valReqNewComment" ValidationGroup="newComment" runat="server" ErrorMessage="Please enter a comment to add." ControlToValidate="txtNewComment" Display="Dynamic" Font-Names="Lato" Font-Size="24px" CssClass="validation" ForeColor="Red"></asp:RequiredFieldValidator>

    <asp:LinkButton ID="btnNewComment" Visible="false" OnClick="btnNewComment_Click" ValidationGroup="newComment" CssClass="newCommentButton" runat="server" Font-Underline="False" Font-Size="24px" ForeColor="#FFCD5C5C" Font-Names="Lato">Add Comment</asp:LinkButton>





    <div class="lineBreak"></div>

    <asp:GridView runat="server" ShowHeader="False" CssClass="gridView" AutoGenerateColumns="False" DataSourceID="dgvCommentsSource" BorderStyle="None" ID="dgvComments" BorderWidth="0px" CellSpacing="5" Font-Names="Lato" Font-Size="24px" GridLines="None" Width="100%" OnDataBound="dgvComments_DataBound" AllowPaging="True" PageSize="100">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <div class="commentContainer">
                        <asp:Label ID="lblCommentID" runat="server" Visible="false" Text='<%# Eval("comment_ID") %>' Font-Names="Lato"></asp:Label>
                        <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("Id") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblSortKey" runat="server" Text='<%# Eval("sortKey") %>' Visible="false"></asp:Label>


                        <asp:Label ID="lblName" CssClass="name" runat="server" Text='<%# Eval("Forename") + " " + Eval("Surname") %>' Font-Bold="True" Font-Names="Lato"></asp:Label>
                        <asp:Label ID="lblTime" CssClass="time" runat="server" Text='<%# Eval("comment_Time") %>' ForeColor="#505050" Font-Size="22px" Font-Names="Lato"></asp:Label>
                        <asp:Label ID="lblComment" CssClass="comment commentItem" runat="server" Text='<%# Eval("comment_Txt") %>' Font-Names="Lato"></asp:Label>

                        <div class="miniLineBreak"></div>
                        <asp:LinkButton ID="btnReply" OnClick="btnReply_Click" CssClass="reply" runat="server" Font-Underline="False" ForeColor="#FFCD5C5C" Font-Names="Lato">Reply </asp:LinkButton>
                        <asp:LinkButton ID="btnDeleteComment" OnClick="btnDeleteComment_Click" CssClass="delete" runat="server" Font-Underline="False" ForeColor="#505050" Font-Names="Lato" Visible="false">Delete</asp:LinkButton>

                        <asp:TextBox ID="txtComment" ValidationGroup="reply" Visible="false" CssClass="textbox commentItem" runat="server" Font-Names="Lato" Font-Size="22px" Width="80%" Height="70px" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="val200LengthNewComment" Display="Dynamic" runat="server" ErrorMessage="Please enter a comment less than 200 characters." ControlToValidate="txtComment" ValidationExpression="^[\s\S]{0,200}$" ValidationGroup="newComment" CssClass="validation"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="valReqReply" ValidationGroup="reply" runat="server" ErrorMessage="Please enter a comment to reply." ControlToValidate="txtComment" Display="Dynamic" Font-Names="Lato" Font-Size="24px" CssClass="validation" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:LinkButton ID="btnComment" Font-Size="22px" Visible="false" ValidationGroup="reply" CssClass="add" runat="server" Font-Underline="False" ForeColor="#FFCD5C5C" Font-Names="Lato" OnClick="btnComment_Click">Add</asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" Font-Size="22px" OnClick="btnCancel_Click" Visible="false" CssClass="cancel" runat="server" Font-Underline="False" ForeColor="#505050" Font-Names="Lato">Cancel</asp:LinkButton>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Position="TopAndBottom" />
    </asp:GridView>



    <asp:SqlDataSource ID="dgvCommentsSource" runat="server" ConnectionString="<%$ ConnectionStrings:NewerDatabase %>" SelectCommand="SELECT AspNetUsers.Id, tblComments.comment_ID, tblComments.comment_Txt, tblComments.comment_Time, tblComments.user_ID, tblComments.sortKey, tblSubmission.submission_ID, AspNetUsers.Forename, AspNetUsers.Surname FROM AspNetUsers INNER JOIN tblComments ON AspNetUsers.Id = tblComments.user_ID INNER JOIN tblSubmission_Comments ON tblComments.comment_ID = tblSubmission_Comments.comment_ID INNER JOIN tblSubmission ON tblSubmission_Comments.submission_ID = tblSubmission.submission_ID WHERE (tblSubmission.submission_ID = @submission_ID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="submission_ID" QueryStringField="submission" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="FormViewDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:NewerDatabase %>" SelectCommand="SELECT tblSubmission.submission_ID, tblSubmission.submission_Title, tblSubmission.submission_LastEdited, tblSubmission.submission_Status, tblSubmission.submission_SubmitDate, AspNetUsers.Id, AspNetUsers.Forename, AspNetUsers.Surname, AspNetUsers.Email FROM tblSubmission INNER JOIN tblUser_Submission ON tblSubmission.submission_ID = tblUser_Submission.submission_ID INNER JOIN AspNetUsers ON tblUser_Submission.Id = AspNetUsers.Id WHERE (tblSubmission.submission_ID = @submission_ID)">
        <SelectParameters>
            <asp:QueryStringParameter DbType="Int32" Name="submission_ID" QueryStringField="submission" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
