﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FSEREC.student
{
    public partial class newSubmission : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (uplForm.FileName == uplBrief.FileName || uplReferences.FileName == uplBrief.FileName || uplForm.FileName == uplReferences.FileName)
            {

                SubLit.Text = "<span style = 'color:red; margin-top: 10px; display: block; font-size: 22px; margin-bottom: 10px;' >You have uploaded files with the same names. Please rename your files and try again.</ span >";

            }
            else
            {

                var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];

                using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
                {
                    if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        Sqlconnect.Open();
                        SqlCommand cmd = new SqlCommand("INSERT INTO tblSubmission (submission_Title, submission_LastEdited, submission_Status, submission_SubmitDate) OUTPUT INSERTED.submission_ID VALUES (@Title, @LastEdited, @Status, @SubmitDate)");
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = Sqlconnect;

                        cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                        cmd.Parameters.AddWithValue("@LastEdited", DateTime.Now);
                        cmd.Parameters.AddWithValue("@Status", "Open");
                        cmd.Parameters.AddWithValue("@SubmitDate", DateTime.Now);

                        Int32 SubmissionId = (Int32)cmd.ExecuteScalar();


                        cmd.CommandText = "INSERT INTO tblUser_Submission (Id, submission_ID) VALUES (@Id, @submission_ID)";

                        cmd.Parameters.AddWithValue("@Id", User.Identity.GetUserId());
                        cmd.Parameters.AddWithValue("@submission_ID", SubmissionId);

                        cmd.ExecuteNonQuery();

                        //Application Form ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                        cmd.Parameters.Clear();
                        string current = DateTime.Now.ToString("yyyyMMddHHmmss");

                        string path = Server.MapPath("~/Files/");


                        path = path + current + "/";
                        Directory.CreateDirectory(path);

                        string saveLocation = ("/Files/" + current + "/" + uplForm.FileName);
                        string fileExtension = System.IO.Path.GetExtension(uplForm.FileName);
                        File.Delete(Server.MapPath(saveLocation));



                        cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";


                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Name", uplForm.FileName);
                        cmd.Parameters.AddWithValue("@file_Size", uplForm.PostedFile.ContentLength);
                        cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                        cmd.Parameters.AddWithValue("@file_Location", saveLocation);
                        cmd.Parameters.AddWithValue("@file_No", 1);

                        Int32 FileId = (Int32)cmd.ExecuteScalar();

                        uplForm.SaveAs(Server.MapPath(saveLocation));

                        cmd.CommandText = "INSERT INTO tblSubmission_File (file_Id, submission_ID) VALUES (@file_ID, @submission_ID)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Id", FileId);
                        cmd.Parameters.AddWithValue("@submission_Id", SubmissionId);

                        cmd.ExecuteNonQuery();

                        //References ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



                        saveLocation = ("/Files/" + current + "/" + uplReferences.FileName);
                        fileExtension = System.IO.Path.GetExtension(uplReferences.FileName);
                        File.Delete(Server.MapPath(saveLocation));

                        cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Name", uplReferences.FileName);
                        cmd.Parameters.AddWithValue("@file_Size", uplReferences.PostedFile.ContentLength);
                        cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                        cmd.Parameters.AddWithValue("@file_Location", saveLocation);
                        cmd.Parameters.AddWithValue("@file_No", 2);

                        FileId = (Int32)cmd.ExecuteScalar();

                        uplReferences.SaveAs(Server.MapPath(saveLocation));

                        cmd.CommandText = "INSERT INTO tblSubmission_File (file_Id, submission_ID) VALUES (@file_ID, @submission_ID)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_ID", FileId);
                        cmd.Parameters.AddWithValue("@submission_ID", SubmissionId);

                        cmd.ExecuteNonQuery();

                        //Brief CV /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                        saveLocation = ("/Files/" + current + "/" + uplBrief.FileName);
                        fileExtension = System.IO.Path.GetExtension(uplBrief.FileName);
                        File.Delete(Server.MapPath(saveLocation));

                        cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Name", uplBrief.FileName);
                        cmd.Parameters.AddWithValue("@file_Size", uplBrief.PostedFile.ContentLength);
                        cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                        cmd.Parameters.AddWithValue("@file_Location", saveLocation);
                        cmd.Parameters.AddWithValue("@file_No", 3);

                        FileId = (Int32)cmd.ExecuteScalar();

                        uplBrief.SaveAs(Server.MapPath(saveLocation));

                        cmd.CommandText = "INSERT INTO tblSubmission_File (file_Id, submission_ID) VALUES (@file_ID, @submission_ID)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Id", FileId);
                        cmd.Parameters.AddWithValue("@submission_Id", SubmissionId);

                        cmd.ExecuteNonQuery();

                        //Supporting Documents ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        if (uplSupport.FileName != "")
                        {
                            saveLocation = ("/Files/" + current + "/" + uplSupport.FileName);
                            fileExtension = System.IO.Path.GetExtension(uplSupport.FileName);
                            File.Delete(Server.MapPath(saveLocation));

                            cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";

                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@file_Name", uplSupport.FileName);
                            cmd.Parameters.AddWithValue("@file_Size", uplSupport.PostedFile.ContentLength);
                            cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                            cmd.Parameters.AddWithValue("@file_Location", saveLocation);
                            cmd.Parameters.AddWithValue("@file_No", 4);

                            FileId = (Int32)cmd.ExecuteScalar();

                            uplSupport.SaveAs(Server.MapPath(saveLocation));

                            cmd.CommandText = "INSERT INTO tblSubmission_File (file_Id, submission_ID) VALUES (@file_ID, @submission_ID)";

                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@file_Id", FileId);
                            cmd.Parameters.AddWithValue("@submission_Id", SubmissionId);

                            cmd.ExecuteNonQuery();




                        }

                        Sqlconnect.Close();


                        //SmtpClient client = new SmtpClient();
                        //client.Host = "smtp.gmail.com";
                        //client.Port = 587;
                        //client.EnableSsl = true;
                        //System.Net.NetworkCredential userpass = new System.Net.NetworkCredential();
                        //userpass.UserName = "fserecapplication@gmail.com";
                        //userpass.Password = "FSEREC123";

                        //client.Credentials = userpass;

                        //MailMessage msg = new MailMessage("fserecapplication@gmail.com", "1515295@chester.ac.uk");

                        //string firstname = "";
                        //string surname = "";


                        //string userId = Context.User.Identity.GetUserId();

                        //Sqlconnect.Open();
                        //cmd.CommandText = "SELECT Forename, Surname FROM AspNetUsers WHERE Id = '" + userId + "'";

                        //SqlDataReader dr = cmd.ExecuteReader();
                        //while (dr.Read())
                        //{
                        //    firstname = dr["Forename"].ToString();
                        //    surname = dr["Surname"].ToString();
                        //}
                        //Sqlconnect.Close();


                        //msg.Subject = "New FSE REC Application has been submitted.";
                        //msg.Body = "New submission submitted by: " + firstname + " " + surname + " (" + HttpContext.Current.User.Identity.Name + "). Their application can be found at https://rec.fse.network/submissionDetail?submission=" + SubmissionId;

                        //client.Send(msg);

                        Response.Redirect("/signOut.aspx?signout=submit");


                    }

                }
            }


        }
    }
}





