﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using System.Security.Claims;
using FSEREC.Models;
using Microsoft.SqlServer.Server;
using System.Data.SqlClient;
using System.Data;

namespace FSEREC.Account
{
    public partial class ManagePassword : System.Web.UI.Page
    {
        protected void btnupdate_Click(object sender, EventArgs e)
        {
            var identityDbContext = new IdentityDbContext("NewerDatabase");
            var userStore = new UserStore<IdentityUser>(identityDbContext);
            var userManager = new UserManager<IdentityUser>(userStore);

            string str = System.Web.HttpContext.Current.User.Identity.GetUserName();

            var user = userManager.Find(str, txtcurrentpass.Text);


            if (user != null)
            {

                userManager.RemovePassword(user.Id);
                userManager.AddPassword(user.Id, txtnewpass.Text);

                ltrPassword.Text = "<span style = 'color:royalblue; margin-top: 10px; display: block; font-size: 22px; margin-bottom: 10px;'>Password successfully changed!</span>";

            }

        }

        protected void btnemailupdate_Click(object sender, EventArgs e)
        {
            var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new IdentityDbContext("NewerDatabase")));
            var user = userManager.FindByName(Context.User.Identity.GetUserName());
            string userId = Context.User.Identity.GetUserId();
            
            {
                user.Email = txtEmail.Text;
                user.UserName = txtEmail.Text;
                userManager.Update(user);

                ltrEmail.Text = "<span style = 'color:royalblue; margin-top: 10px; display: block; font-size: 22px; margin-bottom: 10px;'>Email successfully changed!</span>";
            }
        }

    }
}





