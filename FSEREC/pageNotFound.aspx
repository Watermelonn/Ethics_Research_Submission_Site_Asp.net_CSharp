﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="pageNotFound.aspx.cs" Inherits="FSEREC.error404" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

     <style type="text/css">

        p {
            font-size:22px;
        }

        .fail {
            width: 500px
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <h1 class="title">Page not found!</h1>
    <div class="line"></div>

    <p>We couldn't find the page you requested! Either it was deleted or doesn't exist.</p>


</asp:Content>
