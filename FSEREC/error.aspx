﻿<%@ Page Title="It's all gone wrong!" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="FSEREC.error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">

        p {
            font-size:22px;
        }

        .fail {
            width: 500px
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1 class="title">Oops!</h1>
    <div class="line"></div>

    <p>It's seems like we're having trouble displaying this page. Please try again in a couple of seconds!</p>

    <p>Don't worry! We're doing everything we can.</p>

    <p>More Info:</p>
    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
</asp:Content>
