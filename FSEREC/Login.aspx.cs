﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Data.Entity.Validation;
using System.DirectoryServices.AccountManagement;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.DirectoryServices;
using System.Text;
using System.Collections;

namespace FSERec
{
    public partial class Login : System.Web.UI.Page
    {
        bool remember = new bool();




        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private void LoginUserIn(UserManager<IdentityUser> usermanager, IdentityUser user)
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            var userIdentity = usermanager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = remember }, userIdentity);
        }


        protected void RegBtn_Click(object sender, EventArgs e)
        {
            var identityDbContext = new IdentityDbContext("NewerDatabase");
            var userStore = new UserStore<IdentityUser>(identityDbContext);
            var manager = new UserManager<IdentityUser>(userStore);
            var user = new IdentityUser() { UserName = EmailRegBox.Text, Email = EmailRegBox.Text };
            IdentityResult result = manager.Create(user, PassRegBox.Text);

            if (result.Succeeded)
            {
                var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
                using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
                {
                    Sqlconnect.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "UPDATE AspNetUsers SET Institution = @Institution, Forename = @Forename, Surname = @Surname WHERE Id = @Id;";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = Sqlconnect;

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@Id", user.Id);
                    cmd.Parameters.AddWithValue("@Institution", RegOrg.Text);
                    cmd.Parameters.AddWithValue("@Forename", FirstRegBox.Text);
                    cmd.Parameters.AddWithValue("@Surname", LastRegBox.Text);

                    cmd.ExecuteNonQuery();

                    Sqlconnect.Close();
                }


                manager.AddToRole(user.Id, "User");
                manager.Update(user);


                RegLit.Text = "<span style='color:royalblue; margin-top: 10px; font-size: 22px; margin-bottom: 10px;'>Registration successful! Please login to complete registration.</span>";


            }
            else
            {
                if (result.Errors.FirstOrDefault() == "Passwords must be at least 6 characters.")
                {
                    RegLit.Text = "<span style='color:red; margin-top: 10px; margin-bottom: 10px; font-size: 22px;'>An error has occurred: " + result.Errors.FirstOrDefault() + "</ span >";
                }
            }
        }



        protected void Loginbtn_Click(object sender, EventArgs e)
        {
            var identityDbContext = new IdentityDbContext("NewerDatabase");
            var userStore = new UserStore<IdentityUser>(identityDbContext);
            var userManager = new UserManager<IdentityUser>(userStore);
            var user = userManager.Find(Emailtxt.Text, Passwordtxt.Text);
            if (user != null)
            {

                remember = cbxRemember.Checked;
                LoginUserIn(userManager, user);
                string ReturnUrl = Request.QueryString["ReturnUrl"];

                if (ReturnUrl != null)
                {
                    Response.Redirect(ReturnUrl);
                }
                else
                {


                    Response.Redirect("/signOut.aspx?signout=false");
                }

            }

            else
            {

                bool valid = false;

                //using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName, Emailtxt.Text, Passwordtxt.Text))
                //{

                //    valid = pc.ValidateCredentials("COMPSCI\\" + Emailtxt.Text, Passwordtxt.Text, ContextOptions.SimpleBind);

                //}

                if (valid && Passwordtxt.Text != "")
                {


                    string userExist = "";

                    var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
                    using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
                    {

                        Sqlconnect.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "SELECT Id FROM AspNetUsers WHERE UserName = '" + Emailtxt.Text + "'";
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = Sqlconnect;

                        SqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            userExist = dr["Id"].ToString();
                        }

                        dr.Close();

                        var newUser = new IdentityUser();

                        if (userExist == "")
                        {




                            newUser.UserName = Emailtxt.Text;

                            newUser.UserName = Emailtxt.Text;
                            IdentityResult result = userManager.Create(newUser);

                            if (result.Succeeded)
                            {

                                string firstname;
                                string surname;
                                string email;
                                ArrayList roles = new ArrayList();

                                using (var context = new PrincipalContext(ContextType.Domain, System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName))
                                {
                                    var principal = UserPrincipal.FindByIdentity(context, newUser.UserName);
                                    firstname = principal.GivenName;
                                    surname = principal.Surname;
                                    email = principal.EmailAddress;
                                    foreach (var group in principal.GetAuthorizationGroups())
                                    {
                                        roles.Add(group);
                                    }
                                }

                                cmd.CommandText = "UPDATE AspNetUsers SET Forename = @Forename, Surname = @Surname, Institution = @Institution WHERE Id = @Id;";
                                cmd.CommandType = CommandType.Text;
                                cmd.Connection = Sqlconnect;

                                cmd.Parameters.Clear();
                                cmd.Parameters.AddWithValue("@Id", newUser.Id);
                                cmd.Parameters.AddWithValue("@Forename", firstname);
                                cmd.Parameters.AddWithValue("@Surname", surname);
                                cmd.Parameters.AddWithValue("@Institution", "University of Chester");

                                cmd.ExecuteNonQuery();
                                


                                userManager.AddToRole(newUser.Id, "User");
                                newUser.Email = email;
                                userManager.Update(newUser);

                            }
                        }
                        Sqlconnect.Close();


                        remember = cbxRemember.Checked;

                        newUser = userManager.FindByName(Emailtxt.Text);

                        var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                        var userIdentity = userManager.CreateIdentity(newUser, DefaultAuthenticationTypes.ApplicationCookie);
                        authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = remember }, userIdentity);

                        string ReturnUrl = Request.QueryString["ReturnUrl"];


                        if (ReturnUrl != null)
                        {
                            Response.Redirect(ReturnUrl);
                        }
                        else
                        {
                            Response.Redirect("/signOut.aspx?signout=false");
                        }
                    }
                }

                else
                {
                    Loginlit.Text = "<span style='color:red; margin-top: 10px; margin-bottom: 10px; font-size: 22px;'>Incorrect Email Address or Password. Please try again.</span>";
                }







            }
        }
    }
}


