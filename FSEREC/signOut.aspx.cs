﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FSEREC
{
    public partial class signOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string signOut = Request.QueryString["signout"];

            if (signOut == "false")
            {
                lblSuccess.Text = "You have been logged in!";
            }

            else if (signOut == "true")
            {
                lblSuccess.Text = "You have been successfully signed out!";
            }

            else if (signOut == "submit")
            {
                lblSuccess.Text = "You have successfully submitted the application! You can view a full list of submissions by clicking the My Submissions tab at the top of the page.";
            }
            
            else
            {
                lblSuccess.Text = "You have broken the system!";
            }

        }
    }
}