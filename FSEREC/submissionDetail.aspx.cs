﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Checksums;

namespace FSEREC.student
{
    public partial class submissonDetail : System.Web.UI.Page
    {
        string[] file_ID = new string[4];
        string[] file_Name = new string[4];
        string[] file_Size = new string[4];
        string[] file_Type = new string[4];
        string[] file_No = new string[4];
        string[] file_Location = new string[4];
        string submission_Title;
        string user_Email;

        string appFile_ID;
        string refFile_ID;
        string CVFile_ID;
        string supFile_ID;

        string appFile_Name;
        string refFile_Name;
        string CVFile_Name;
        string supFile_Name;

        string appFile_Location;
        string refFile_Location;
        string CVFile_Location;
        string supFile_Location;

        int submission_ID;

        int levelNumber;
        string firstValue;
        string secondValue;

        protected void Page_Load(object sender, EventArgs e)
        {

            System.Web.UI.WebControls.Label lblId = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblId");
            System.Web.UI.WebControls.Label lblName = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblName");
            System.Web.UI.WebControls.Label lblEmail = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblEmail");

            string userID = "";

            if (lblId != null)
            {
                string queryString = Request.QueryString["submission"];
                if (queryString != null)
                {


                    if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        string userId = User.Identity.GetUserId();
                        var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new IdentityDbContext("NewerDatabase")));


                        if (userManager.IsInRole(userId, "Admin") | userManager.IsInRole(userId, "Committee"))
                        {
                            submissionDetail.FindControl("btnEdit").Visible = false;
                            submissionDetail.FindControl("btnDeleteSubmission").Visible = true;
                            submissionDetail.FindControl("btnStatus").Visible = true;
                            lblComments.Visible = true;
                            txtNewComment.Visible = true;
                            btnNewComment.Visible = true;
                            dgvComments.Visible = true;
                            
                        }

                        else if (userManager.IsInRole(userId, "User"))
                        {
                            userID = lblId.Text;

                            if (userID != HttpContext.Current.User.Identity.GetUserId())
                            {
                                Response.Redirect("/forbidden.aspx");
                            }
                            lblComments.Visible = false;
                            dgvComments.Visible = false;


                            lblName.Visible = false;
                            lblEmail.Visible = false;


                        }
                    }
                    else
                    {
                        Response.Redirect("/forbidden.aspx");
                    }


                    System.Web.UI.WebControls.Button btnStatus = (System.Web.UI.WebControls.Button)submissionDetail.FindControl("btnStatus");
                    System.Web.UI.WebControls.Label lblStatus = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblStatus");

                    if (lblStatus.Text == "Open")
                    {
                        lblStatus.ForeColor = Color.LimeGreen;
                        btnStatus.Text = "Close Application";
                    }
                    else if (lblStatus.Text == "Closed")
                    {
                        lblStatus.ForeColor = Color.Red;
                        btnStatus.Text = "Reopen Application";
                    }

                    submission_ID = Int32.Parse(queryString);
                    var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];

                    System.Web.UI.WebControls.Label lblTitle = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblTitle");
                    submission_Title = lblTitle.Text;

                    
                    user_Email = lblEmail.Text;

                    int x = new int();

                    using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
                    {
                        Sqlconnect.Open();
                        SqlCommand cmd = new SqlCommand("SELECT tblSubmission.submission_ID, tblFile.file_ID, tblFile.file_Name, tblFile.file_Size, tblFile.file_Location, tblFile.file_No, tblFile.file_Type FROM tblFile INNER JOIN tblSubmission_File ON tblFile.file_ID = tblSubmission_File.file_ID INNER JOIN tblSubmission ON tblSubmission_File.submission_ID = tblSubmission.submission_ID WHERE tblSubmission.submission_ID = " + submission_ID);
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = Sqlconnect;

                        SqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            file_ID[x] = dr["file_ID"].ToString();
                            file_Name[x] = dr["file_Name"].ToString();
                            file_Size[x] = dr["file_Size"].ToString();
                            file_Type[x] = dr["file_Type"].ToString();
                            file_No[x] = dr["file_No"].ToString();
                            file_Location[x] = dr["file_Location"].ToString();
                            x = x + 1;
                        }




                        System.Web.UI.WebControls.Label lblApplicationName = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblApplicationName");
                        System.Web.UI.WebControls.Label lblCVName = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblCVName");
                        System.Web.UI.WebControls.Label lblReferencesName = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblReferencesName");
                        System.Web.UI.WebControls.Label lblSupportingName = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblSupportingName");


                        lblSupportingName.Text = "No file";
                        submissionDetail.FindControl("btnDownload4").Visible = false;

                        for (int y = 0; y < 4; y = y + 1)
                        {
                            if (file_No[y] == "1")
                            {
                                appFile_ID = file_ID[y];
                                lblApplicationName.Text = file_Name[y];
                                appFile_Name = file_Name[y];
                                appFile_Location = file_Location[y];
                            }
                            if (file_No[y] == "2")
                            {
                                refFile_ID = file_ID[y];
                                lblReferencesName.Text = file_Name[y];
                                refFile_Name = file_Name[y];
                                refFile_Location = file_Location[y];
                            }
                            if (file_No[y] == "3")
                            {
                                CVFile_ID = file_ID[y];
                                lblCVName.Text = file_Name[y];
                                CVFile_Name = file_Name[y];
                                CVFile_Location = file_Location[y];
                            }
                            if (file_No[y] == "4")
                            {

                                supFile_ID = file_ID[y];
                                lblSupportingName.Text = file_Name[y];
                                supFile_Name = file_Name[y];
                                supFile_Location = file_Location[y];
                                submissionDetail.FindControl("btnDownload4").Visible = true;
                            }

                        }


                        Sqlconnect.Close();

                    }
                }

            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            enableButtons();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            disableButtons();
        }



        protected void btnReplace1_Click(object sender, EventArgs e)
        {

            submissionDetail.FindControl("uplApplication").Visible = true;
            submissionDetail.FindControl("btnDownload1").Visible = false;
            submissionDetail.FindControl("lblApplicationName").Visible = false;
            submissionDetail.FindControl("btnReplace1").Visible = false;
            submissionDetail.FindControl("btnSave1").Visible = true;
            submissionDetail.FindControl("btnCancel1").Visible = true;

        }


        protected void btnDeleteSubmission_Click(object sender, EventArgs e)
        {
            List<string> comment_ID = new List<string>();

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.CommandText = "DELETE tblSubmission_File WHERE submission_ID = " + submission_ID;

                cmd.ExecuteNonQuery();

                cmd.CommandText = "DELETE tblUser_Submission WHERE submission_ID = " + submission_ID;

                cmd.ExecuteNonQuery();

                if (file_ID[3] != null)
                {
                    cmd.CommandText = "DELETE tblFile WHERE file_ID = " + file_ID[0] + " OR file_ID = " + file_ID[1] + " OR file_ID = " + file_ID[2] + " OR file_ID = " + file_ID[3];
                }
                else
                {
                    cmd.CommandText = "DELETE tblFile WHERE file_ID = " + file_ID[0] + " OR file_ID = " + file_ID[1] + " OR file_ID = " + file_ID[2];
                }

                cmd.ExecuteNonQuery();

                cmd.CommandText = "SELECT comment_ID FROM tblSubmission_Comments WHERE submission_ID = '" + submission_ID + "'";

                try
                {
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        comment_ID.Add(dr["comment_ID"].ToString());
                    }

                    dr.Close();
                }
                catch
                {

                }

                cmd.CommandText = "DELETE tblSubmission_Comments WHERE submission_ID = '" + submission_ID + "'";

                cmd.ExecuteNonQuery();

                foreach (string ID in comment_ID)
                {

                    cmd.CommandText = "DELETE tblComments WHERE sortKey LIKE '" + ID + "'";

                    cmd.ExecuteNonQuery();

                }

                File.Delete(Server.MapPath(file_Location[0]));
                File.Delete(Server.MapPath(file_Location[1]));
                File.Delete(Server.MapPath(file_Location[2]));
                File.Delete(Server.MapPath(file_Location[3]));

                cmd.CommandText = "DELETE tblSubmission WHERE submission_ID = " + submission_ID;

                cmd.ExecuteNonQuery();

                Sqlconnect.Close();

            }


            Response.Redirect("/Committee/viewSubmissions.aspx");


        }

        protected void btnCancel1_Click(object sender, EventArgs e)
        {

            submissionDetail.FindControl("uplApplication").Visible = false;
            submissionDetail.FindControl("btnDownload1").Visible = true;
            submissionDetail.FindControl("lblApplicationName").Visible = true;
            submissionDetail.FindControl("btnReplace1").Visible = true;
            submissionDetail.FindControl("btnSave1").Visible = false;
            submissionDetail.FindControl("btnCancel1").Visible = false;


        }


        protected void btnSave1_Click(object sender, EventArgs e)
        {

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                FileUpload uplApplication = (FileUpload)submissionDetail.FindControl("uplApplication");

                if (uplApplication.FileName != "")
                {

                    if (uplApplication.FileName == CVFile_ID || uplApplication.FileName == refFile_Name)
                    {

                        Literal ltrFileError = (Literal)submissionDetail.FindControl("ltrFileError");

                        ltrFileError.Text = "<span style = 'color:red; margin-top: 10px; display: block; font-size: 22px; margin-bottom: 10px;'>You cannot upload this file because your submission contains a file with the same name. Please rename your file and try again.</span>";

                    }

                    else
                    {
                        Sqlconnect.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "DELETE tblSubmission_File WHERE submission_ID = " + submission_ID + " AND file_ID = " + appFile_ID;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = Sqlconnect;

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "DELETE tblFile WHERE file_ID = " + appFile_ID;

                        cmd.ExecuteNonQuery();

                        File.Delete(Server.MapPath(appFile_Location));

                        string saveLocation;
                        saveLocation = Path.GetDirectoryName(appFile_Location);
                        string fileExtension = System.IO.Path.GetExtension(uplApplication.FileName);

                        cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Name", uplApplication.FileName);
                        cmd.Parameters.AddWithValue("@file_Size", uplApplication.PostedFile.ContentLength);
                        cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                        cmd.Parameters.AddWithValue("@file_Location", saveLocation + "/" + uplApplication.FileName);
                        cmd.Parameters.AddWithValue("@file_No", 1);

                        Int32 FileId = (Int32)cmd.ExecuteScalar();

                        uplApplication.SaveAs(Server.MapPath(saveLocation + "/" + uplApplication.FileName));

                        cmd.CommandText = "INSERT INTO tblSubmission_File (file_ID, submission_ID) VALUES (@file_ID, @submission_ID)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_ID", FileId);
                        cmd.Parameters.AddWithValue("@submission_Id", submission_ID);

                        cmd.ExecuteNonQuery();

                        Sqlconnect.Close();

                        btnCancel1_Click(sender, e);
                        Page_Load(sender, e);
                    }
                }

            }

        }

        protected void btnReplace2_Click(object sender, EventArgs e)
        {

            submissionDetail.FindControl("uplCV").Visible = true;
            submissionDetail.FindControl("btnDownload2").Visible = false;
            submissionDetail.FindControl("lblCVName").Visible = false;
            submissionDetail.FindControl("btnReplace2").Visible = false;
            submissionDetail.FindControl("btnSave2").Visible = true;
            submissionDetail.FindControl("btnCancel2").Visible = true;

        }

        protected void btnCancel2_Click(object sender, EventArgs e)
        {
            submissionDetail.FindControl("uplCV").Visible = false;
            submissionDetail.FindControl("btnDownload2").Visible = true;
            submissionDetail.FindControl("lblCVName").Visible = true;
            submissionDetail.FindControl("btnReplace2").Visible = true;
            submissionDetail.FindControl("btnCancel2").Visible = false;
            submissionDetail.FindControl("btnSave2").Visible = false;
        }

        protected void btnSave2_Click(object sender, EventArgs e)
        {

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                FileUpload uplCV = (FileUpload)submissionDetail.FindControl("uplCV");

                if (uplCV.FileName != "")
                {

                    if (uplCV.FileName == refFile_ID || uplCV.FileName == appFile_Name)
                    {

                        Literal ltrFileError = (Literal)submissionDetail.FindControl("ltrFileError");

                        ltrFileError.Text = "<span style = 'color:red; margin-top: 10px; display: block; font-size: 22px; margin-bottom: 10px;'>You cannot upload this file because your submission contains a file with the same name. Please rename your file and try again.</span>";

                    }

                    else
                    {
                        Sqlconnect.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "DELETE tblSubmission_File WHERE submission_ID = " + submission_ID + " AND file_ID = " + CVFile_ID;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = Sqlconnect;

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "DELETE tblFile WHERE file_ID = " + CVFile_ID;

                        cmd.ExecuteNonQuery();

                        File.Delete(Server.MapPath(CVFile_Location));

                        string saveLocation;
                        saveLocation = Path.GetDirectoryName(CVFile_Location);
                        string fileExtension = System.IO.Path.GetExtension(uplCV.FileName);

                        cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Name", uplCV.FileName);
                        cmd.Parameters.AddWithValue("@file_Size", uplCV.PostedFile.ContentLength);
                        cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                        cmd.Parameters.AddWithValue("@file_Location", saveLocation + "/" + uplCV.FileName);
                        cmd.Parameters.AddWithValue("@file_No", 3);

                        Int32 FileId = (Int32)cmd.ExecuteScalar();

                        uplCV.SaveAs(Server.MapPath(saveLocation + "/" + uplCV.FileName));

                        cmd.CommandText = "INSERT INTO tblSubmission_File (file_ID, submission_ID) VALUES (@file_ID, @submission_ID)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_ID", FileId);
                        cmd.Parameters.AddWithValue("@submission_Id", submission_ID);

                        cmd.ExecuteNonQuery();

                        Sqlconnect.Close();

                        btnCancel2_Click(sender, e);
                        Page_Load(sender, e);
                    }
                }

            }

        }

        protected void btnReplace3_Click(object sender, EventArgs e)
        {
            submissionDetail.FindControl("uplReferences").Visible = true;
            submissionDetail.FindControl("btnDownload3").Visible = false;
            submissionDetail.FindControl("lblReferencesName").Visible = false;
            submissionDetail.FindControl("btnReplace3").Visible = false;
            submissionDetail.FindControl("btnCancel3").Visible = true;
            submissionDetail.FindControl("btnSave3").Visible = true;
        }

        protected void btnCancel3_Click(object sender, EventArgs e)
        {
            submissionDetail.FindControl("uplReferences").Visible = false;
            submissionDetail.FindControl("btnDownload3").Visible = true;
            submissionDetail.FindControl("lblReferencesName").Visible = true;
            submissionDetail.FindControl("btnReplace3").Visible = true;
            submissionDetail.FindControl("btnCancel3").Visible = false;
            submissionDetail.FindControl("btnSave3").Visible = false;
        }

        protected void btnSave3_Click(object sender, EventArgs e)
        {
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                FileUpload uplReferences = (FileUpload)submissionDetail.FindControl("uplReferences");

                if (uplReferences.FileName != "")
                {

                    if (uplReferences.FileName == CVFile_ID || uplReferences.FileName == appFile_Name)
                    {

                        Literal ltrFileError = (Literal)submissionDetail.FindControl("ltrFileError");

                        ltrFileError.Text = "<span style = 'color:red; margin-top: 10px; display: block; font-size: 22px; margin-bottom: 10px;'>You cannot upload this file because your submission contains a file with the same name. Please rename your file and try again.</span>";

                    }

                    else
                    {
                        Sqlconnect.Open();
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = "DELETE tblSubmission_File WHERE submission_ID = " + submission_ID + " AND file_ID = " + refFile_ID;
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = Sqlconnect;

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "DELETE tblFile WHERE file_ID = " + refFile_ID;

                        cmd.ExecuteNonQuery();

                        File.Delete(Server.MapPath(refFile_Location));

                        string saveLocation;
                        saveLocation = Path.GetDirectoryName(refFile_Location);
                        string fileExtension = System.IO.Path.GetExtension(uplReferences.FileName);

                        cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_Name", uplReferences.FileName);
                        cmd.Parameters.AddWithValue("@file_Size", uplReferences.PostedFile.ContentLength);
                        cmd.Parameters.AddWithValue("@file_Type", fileExtension);
                        cmd.Parameters.AddWithValue("@file_Location", saveLocation + "/" + uplReferences.FileName);
                        cmd.Parameters.AddWithValue("@file_No", 2);

                        Int32 FileId = (Int32)cmd.ExecuteScalar();

                        uplReferences.SaveAs(Server.MapPath(saveLocation + "/" + uplReferences.FileName));

                        cmd.CommandText = "INSERT INTO tblSubmission_File (file_ID, submission_ID) VALUES (@file_ID, @submission_ID)";

                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@file_ID", FileId);
                        cmd.Parameters.AddWithValue("@submission_Id", submission_ID);

                        cmd.ExecuteNonQuery();

                        Sqlconnect.Close();

                        btnCancel3_Click(sender, e);
                        Page_Load(sender, e);
                    }
                }

            }
        }

        protected void btnReplace4_Click(object sender, EventArgs e)
        {
            submissionDetail.FindControl("uplSupporting").Visible = true;
            submissionDetail.FindControl("btnDownload4").Visible = false;
            submissionDetail.FindControl("lblSupportingName").Visible = false;
            submissionDetail.FindControl("btnReplace4").Visible = false;
            submissionDetail.FindControl("btnDelete").Visible = false;
            submissionDetail.FindControl("btnCancel4").Visible = true;
            submissionDetail.FindControl("btnSave4").Visible = true;
        }

        protected void btnCancel4_Click(object sender, EventArgs e)
        {
            submissionDetail.FindControl("uplSupporting").Visible = false;
            submissionDetail.FindControl("btnDownload4").Visible = true;
            submissionDetail.FindControl("lblSupportingName").Visible = true;
            submissionDetail.FindControl("btnReplace4").Visible = true;
            submissionDetail.FindControl("btnDelete").Visible = true;
            submissionDetail.FindControl("btnCancel4").Visible = false;
            submissionDetail.FindControl("btnSave4").Visible = false;
        }
        protected void btnSave4_Click(object sender, EventArgs e)
        {
            string saveLocation = "";
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                FileUpload uplSupporting = (FileUpload)submissionDetail.FindControl("uplSupporting");

                if (uplSupporting.FileName != "")
                {
                    Sqlconnect.Open();
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = Sqlconnect;



                    if (supFile_Name != null)
                    {
                        cmd.CommandText = "DELETE tblSubmission_File WHERE submission_ID = " + submission_ID + " AND file_ID = " + supFile_ID;


                        cmd.ExecuteNonQuery();

                        cmd.CommandText = "DELETE tblFile WHERE file_ID = " + supFile_ID;

                        cmd.ExecuteNonQuery();

                        File.Delete(Server.MapPath(supFile_Location));


                        saveLocation = Path.GetDirectoryName(appFile_Location);
                    }



                    cmd.CommandText = "INSERT INTO tblFile (file_Name, file_Size, file_Type, file_Location, file_No) OUTPUT INSERTED.file_ID VALUES (@file_Name, @file_Size, @file_Type, @file_Location, @file_No)";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@file_Name", uplSupporting.FileName);
                    cmd.Parameters.AddWithValue("@file_Size", uplSupporting.PostedFile.ContentLength);
                    cmd.Parameters.AddWithValue("@file_Type", ".zip");
                    cmd.Parameters.AddWithValue("@file_Location", saveLocation + "/" + uplSupporting.FileName);
                    cmd.Parameters.AddWithValue("@file_No", 4);

                    Int32 FileId = (Int32)cmd.ExecuteScalar();

                    uplSupporting.SaveAs(Server.MapPath(saveLocation + "/" + uplSupporting.FileName));

                    cmd.CommandText = "INSERT INTO tblSubmission_File (file_ID, submission_ID) VALUES (@file_ID, @submission_ID)";

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@file_ID", FileId);
                    cmd.Parameters.AddWithValue("@submission_Id", submission_ID);

                    cmd.ExecuteNonQuery();

                    Sqlconnect.Close();

                    btnCancel4_Click(sender, e);
                    Page_Load(sender, e);

                }

            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.CommandText = "DELETE tblSubmission_File WHERE submission_ID = " + submission_ID + " AND file_ID = " + supFile_ID;


                cmd.ExecuteNonQuery();

                cmd.CommandText = "DELETE tblFile WHERE file_ID = " + supFile_ID;

                cmd.ExecuteNonQuery();
                
                File.Delete(Server.MapPath(supFile_Location));

                Sqlconnect.Close();

                btnCancel4_Click(sender, e);

                Response.Redirect(Request.RawUrl);
            }
        }


        protected void btnEditTitle_Click(object sender, EventArgs e)
        {

            System.Web.UI.WebControls.TextBox txtTitle = (System.Web.UI.WebControls.TextBox)submissionDetail.FindControl("txtTitle");
            System.Web.UI.WebControls.Label lblTitle = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblTitle");

            lblTitle.Visible = false;
            txtTitle.Visible = true;

            submissionDetail.FindControl("btnEditTitle").Visible = false;
            submissionDetail.FindControl("btnCancelTitle").Visible = true;
            submissionDetail.FindControl("btnSaveTitle").Visible = true;

            txtTitle.Text = lblTitle.Text;


        }

        protected void btnCancelTitle_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.TextBox txtTitle = (System.Web.UI.WebControls.TextBox)submissionDetail.FindControl("txtTitle");
            System.Web.UI.WebControls.Label lblTitle = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblTitle");

            lblTitle.Visible = true;
            txtTitle.Visible = false;

            submissionDetail.FindControl("btnEditTitle").Visible = true;
            submissionDetail.FindControl("btnCancelTitle").Visible = false;
            submissionDetail.FindControl("btnSaveTitle").Visible = false;

            lblTitle.Text = txtTitle.Text;

        }


        protected void btnSaveTitle_Click(object sender, EventArgs e)
        {

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            System.Web.UI.WebControls.TextBox txtTitle = (System.Web.UI.WebControls.TextBox)submissionDetail.FindControl("txtTitle");
            System.Web.UI.WebControls.Label lblTitle = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblTitle");

            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {


                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("UPDATE tblSubmission SET submission_Title = @submission_Title WHERE submission_ID = " + submission_ID);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@submission_Title", txtTitle.Text);

                cmd.ExecuteNonQuery();

                Sqlconnect.Close();

            }

            lblTitle.Visible = true;
            txtTitle.Visible = false;

            submissionDetail.FindControl("btnEditTitle").Visible = true;
            submissionDetail.FindControl("btnCancelTitle").Visible = false;
            submissionDetail.FindControl("btnSaveTitle").Visible = false;

            lblTitle.Text = txtTitle.Text;

        }


        protected void btnDownloadAll_Click(object sender, EventArgs e)
        {

            List<string> filesToArchive = new List<string>();

            //Here we are adding two hard-coded files to our list
            filesToArchive.Add(Server.MapPath(file_Location[0]));
            filesToArchive.Add(Server.MapPath(file_Location[1]));
            filesToArchive.Add(Server.MapPath(file_Location[2]));
            if (file_Location[3] != "")
            {
                filesToArchive.Add(Server.MapPath(file_Location[3]));
            }


            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + user_Email + " " + submission_Title + ".zip\"");
            Response.ContentType = "application/x-zip-compressed";

            using (var zipStream = new ZipOutputStream(Response.OutputStream))
            {
                foreach (string filePath in filesToArchive)
                {
                    byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                    var fileEntry = new ZipEntry(Path.GetFileName(filePath))
                    {
                        Size = fileBytes.Length
                    };

                    zipStream.PutNextEntry(fileEntry);
                    zipStream.Write(fileBytes, 0, fileBytes.Length);
                }

                zipStream.Flush();
                zipStream.Close();
            }



        }
        protected void btnDownload1_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file_Name[0] + "\"");
            Response.AddHeader("Content-Length", file_Size[0]);
            Response.ContentType = "application/" + file_Type;
            Response.Flush();
            Response.TransmitFile(Server.MapPath(file_Location[0]));
            Response.End();
        }

        protected void btnDownload2_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file_Name[1] + "\"");
            Response.AddHeader("Content-Length", file_Size[2]);
            Response.ContentType = "application/" + file_Type;
            Response.Flush();
            Response.TransmitFile(Server.MapPath(file_Location[2]));
            Response.End();
        }

        protected void btnDownload3_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file_Name[2] + "\"");
            Response.AddHeader("Content-Length", file_Size[1]);
            Response.ContentType = "application/" + file_Type;
            Response.Flush();
            Response.TransmitFile(Server.MapPath(file_Location[1]));
            Response.End();
        }
        protected void btnDownload4_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file_Name[3] + "\"");
            Response.AddHeader("Content-Length", file_Size[3]);
            Response.ContentType = "application/x-zip-compressed";
            Response.Flush();
            Response.TransmitFile(Server.MapPath(file_Location[3]));
            Response.End();
        }

        protected void btnStatus_Click(object sender, EventArgs e)
        {

            System.Web.UI.WebControls.Button btnStatus = (System.Web.UI.WebControls.Button)submissionDetail.FindControl("btnStatus");
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];

            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                if (btnStatus.Text == "Close Application")
                {




                    Sqlconnect.Open();
                    SqlCommand cmd = new SqlCommand("UPDATE tblSubmission SET submission_Status = @submission_Status WHERE submission_ID = " + submission_ID);
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = Sqlconnect;

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@submission_Status", "Closed");

                    cmd.ExecuteNonQuery();

                    Sqlconnect.Close();

                    Response.Redirect(Request.RawUrl);

                }

                else if (btnStatus.Text == "Reopen Application")
                {

                    Sqlconnect.Open();
                    SqlCommand cmd = new SqlCommand("UPDATE tblSubmission SET submission_Status = @submission_Status WHERE submission_ID = " + submission_ID);
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = Sqlconnect;

                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@submission_Status", "Open");

                    cmd.ExecuteNonQuery();

                    Sqlconnect.Close();
                    Response.Redirect(Request.RawUrl);

                }
            }



        }

        private void disableButtons()
        {

            submissionDetail.FindControl("btnEditTitle").Visible = false;
            submissionDetail.FindControl("btnCancelTitle").Visible = false;
            submissionDetail.FindControl("btnSaveTitle").Visible = false;

            submissionDetail.FindControl("btnReplace1").Visible = false;
            submissionDetail.FindControl("btnReplace2").Visible = false;
            submissionDetail.FindControl("btnReplace3").Visible = false;
            submissionDetail.FindControl("btnReplace4").Visible = false;
            submissionDetail.FindControl("btnDelete").Visible = false;

            submissionDetail.FindControl("btnSave").Visible = false;

            submissionDetail.FindControl("btnSave1").Visible = false;
            submissionDetail.FindControl("btnSave2").Visible = false;
            submissionDetail.FindControl("btnSave3").Visible = false;
            submissionDetail.FindControl("btnSave4").Visible = false;

            submissionDetail.FindControl("btnDownloadAll").Visible = true;
            submissionDetail.FindControl("btnEdit").Visible = true;

            submissionDetail.FindControl("uplApplication").Visible = false;
            submissionDetail.FindControl("uplCV").Visible = false;
            submissionDetail.FindControl("uplReferences").Visible = false;
            submissionDetail.FindControl("uplSupporting").Visible = false;

            submissionDetail.FindControl("btnCancel1").Visible = false;
            submissionDetail.FindControl("btnCancel2").Visible = false;
            submissionDetail.FindControl("btnCancel3").Visible = false;
            submissionDetail.FindControl("btnCancel4").Visible = false;

            submissionDetail.FindControl("lblTitle").Visible = true;
            submissionDetail.FindControl("txtTitle").Visible = false;

            submissionDetail.FindControl("btnDownload1").Visible = true;
            submissionDetail.FindControl("btnDownload2").Visible = true;
            submissionDetail.FindControl("btnDownload3").Visible = true;
            submissionDetail.FindControl("btnDownload4").Visible = true;

            submissionDetail.FindControl("btnDeleteSubmission").Visible = false;

        }

        private void enableButtons()
        {

            System.Web.UI.WebControls.Label lblSupportingName = (System.Web.UI.WebControls.Label)submissionDetail.FindControl("lblSupportingName");



            submissionDetail.FindControl("btnEditTitle").Visible = true;

            submissionDetail.FindControl("btnReplace1").Visible = true;
            submissionDetail.FindControl("btnReplace2").Visible = true;
            submissionDetail.FindControl("btnReplace3").Visible = true;
            submissionDetail.FindControl("btnReplace4").Visible = true;

            if (lblSupportingName.Text != "No file")
            {
                submissionDetail.FindControl("btnDelete").Visible = true;
            }

            submissionDetail.FindControl("btnSave").Visible = true;

            submissionDetail.FindControl("btnDownloadAll").Visible = false;
            submissionDetail.FindControl("btnEdit").Visible = false;
            submissionDetail.FindControl("btnDeleteSubmission").Visible = true;

        }
        

        protected void dgvComments_DataBound(object sender, EventArgs e)
        {


            foreach (GridViewRow row in dgvComments.Rows)
            {
                
                System.Web.UI.WebControls.Label lblSortKey = (System.Web.UI.WebControls.Label)row.FindControl("lblSortKey");

                string[] levels = lblSortKey.Text.Split('.');

                if (levels[1] != "null")
                {
                    row.Cells[0].Style.Add("padding-left", "150px");
                }
                if (levels[2] != "null")
                {
                    row.Cells[0].Style.Add("padding-left", "300px");
                    LinkButton btnReply = (LinkButton)row.FindControl("btnReply");

                    btnReply.Visible = false;


                }

                System.Web.UI.WebControls.Label lblUserID = (System.Web.UI.WebControls.Label)row.FindControl("lblUserID");

                string userId = User.Identity.GetUserId();
                var userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(new IdentityDbContext("NewerDatabase")));

                if (userManager.IsInRole(userId, "Admin"))
                {
                    LinkButton btnDeleteComment = (LinkButton)row.FindControl("btnDeleteComment");
                    btnDeleteComment.Visible = true;
                }

                if (lblUserID.Text == HttpContext.Current.User.Identity.GetUserId())
                {
                    System.Web.UI.WebControls.Label lblName = (System.Web.UI.WebControls.Label)row.FindControl("lblName");
                    LinkButton btnDeleteComment = (LinkButton)row.FindControl("btnDeleteComment");

                    btnDeleteComment.Visible = true;
                    lblName.ForeColor = System.Drawing.Color.DodgerBlue;

                }

            }

            int numberOfRows = dgvComments.Rows.Count;

            if (numberOfRows > 10)
            {
                for (int a = 10; a < numberOfRows; a = a + 1)
                {
                    System.Web.UI.WebControls.Label lblSortKey = (System.Web.UI.WebControls.Label)dgvComments.Rows[a].FindControl("lblSortKey");

                    string[] levels = lblSortKey.Text.Split('.');

                    if (levels[1] == "null" && levels[2] == "null")
                    {

                        dgvComments.PageSize = a;
                        break;

                    }

                }
            }


        }
        protected void btnReply_Click(object sender, EventArgs e)
        {


            LinkButton btnReply = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnReply.NamingContainer;

            LinkButton btnCancel = (LinkButton)row.FindControl("btnCancel");
            System.Web.UI.WebControls.TextBox txtComment = (System.Web.UI.WebControls.TextBox)row.FindControl("txtComment");
            LinkButton btnDeleteComment = (LinkButton)row.FindControl("btnDeleteComment");
            LinkButton btnComment = (LinkButton)row.FindControl("btnComment");

            btnReply.Visible = false;
            btnDeleteComment.Visible = false;
            btnCancel.Visible = true;
            txtComment.Visible = true;
            btnComment.Visible = true;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            LinkButton btnCancel = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnCancel.NamingContainer;

            LinkButton btnReply = (LinkButton)row.FindControl("btnReply");
            System.Web.UI.WebControls.TextBox txtComment = (System.Web.UI.WebControls.TextBox)row.FindControl("txtComment");
            LinkButton btnDeleteComment = (LinkButton)row.FindControl("btnDeleteComment");
            LinkButton btnComment = (LinkButton)row.FindControl("btnComment");

            btnReply.Visible = true;
            btnDeleteComment.Visible = true;
            btnCancel.Visible = false;
            txtComment.Visible = false;
            btnComment.Visible = false;

        }

        protected void btnComment_Click(object sender, EventArgs e)
        {
            LinkButton btnComment = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnComment.NamingContainer;

            System.Web.UI.WebControls.Label lblSortKey = (System.Web.UI.WebControls.Label)row.FindControl("lblSortKey");

            string[] levels = lblSortKey.Text.Split('.');

            if (levels[1] == "null")
            {

                firstValue = levels[0];

                levelNumber = 1;
                addComment(row);
            }
            else if (levels[2] == "null")
            {
                secondValue = levels[1];
                firstValue = levels[0];

                levelNumber = 2;
                addComment(row);
            }


        }

        protected void btnNewComment_Click(object sender, EventArgs e)
        {
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];

            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO tblComments (comment_Txt, comment_Time, user_ID) OUTPUT INSERTED.comment_ID VALUES (@comment_Txt, @comment_Time, @user_ID)");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.Parameters.AddWithValue("@comment_Txt", txtNewComment.Text);
                cmd.Parameters.AddWithValue("@comment_Time", DateTime.Now);
                cmd.Parameters.AddWithValue("@user_ID", HttpContext.Current.User.Identity.GetUserId());

                Int32 comment_ID = (Int32)cmd.ExecuteScalar();

                string sortKey = "";

                sortKey = comment_ID + ".null.null";

                cmd.CommandText = "UPDATE tblComments SET sortKey = @sortKey WHERE comment_ID = " + comment_ID;

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sortKey", sortKey);

                cmd.ExecuteNonQuery();
                
                cmd.CommandText = "INSERT INTO tblSubmission_Comments (comment_ID, submission_ID) VALUES (@comment_ID, @submission_ID)";

 cmd.Parameters.AddWithValue("@comment_ID", comment_ID);
                cmd.Parameters.AddWithValue("@submission_ID", submission_ID);

                cmd.ExecuteNonQuery();
                Sqlconnect.Close();

                Response.Redirect(Request.RawUrl);
            }
        }

        protected void addComment(GridViewRow row)
        {
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];

            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO tblComments (comment_Txt, comment_Time, user_ID) OUTPUT INSERTED.comment_ID VALUES (@comment_Txt, @comment_Time, @user_ID)");
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                System.Web.UI.WebControls.TextBox txtComment = (System.Web.UI.WebControls.TextBox)row.FindControl("txtComment");

                cmd.Parameters.AddWithValue("@comment_Txt", txtComment.Text);
                cmd.Parameters.AddWithValue("@comment_Time", DateTime.Now);
                cmd.Parameters.AddWithValue("@user_ID", HttpContext.Current.User.Identity.GetUserId());

                Int32 comment_ID = (Int32)cmd.ExecuteScalar();

                string sortKey = "";

                if (levelNumber == 1)
                {

                    sortKey = firstValue + "." + comment_ID.ToString() + ".null";
                }
                else if (levelNumber == 2)
                {

                    sortKey = firstValue + "." + secondValue + "." + comment_ID.ToString();
                }

                cmd.CommandText = "UPDATE tblComments SET sortKey = @sortKey WHERE comment_ID = " + comment_ID;

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@sortKey", sortKey);

                cmd.ExecuteNonQuery();
                
                cmd.CommandText = "INSERT INTO tblSubmission_Comments (comment_ID, submission_ID) VALUES (@comment_ID, @submission_ID)";

                cmd.Parameters.AddWithValue("@comment_ID", comment_ID);
                cmd.Parameters.AddWithValue("@submission_ID", submission_ID);

                cmd.ExecuteNonQuery();
                Sqlconnect.Close();

            }
            Response.Redirect(Request.RawUrl);
        }

        protected void btnDeleteComment_Click(object sender, EventArgs e)
        {
            LinkButton btnDeleteComment = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btnDeleteComment.NamingContainer;
            
            System.Web.UI.WebControls.Label lblCommentID = (System.Web.UI.WebControls.Label)row.FindControl("lblCommentID");

            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {

                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                cmd.CommandText = "DELETE tblComments WHERE sortKey LIKE '%" + lblCommentID.Text + "%'";

                cmd.ExecuteNonQuery();
              
                Sqlconnect.Close();

            }


            Response.Redirect(Request.RawUrl);
        }
        

    }
}



