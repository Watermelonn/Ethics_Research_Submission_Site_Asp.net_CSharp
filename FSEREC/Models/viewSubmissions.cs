﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FSEREC.Models
{
    public class viewSubmissions
    {
        static DatabaseEntities db = new DatabaseEntities();

        public static IEnumerable<tblSubmission> GetAllSubmissions()
        {
            var result = from r in db.tblSubmissions select r;
            return result;
        }

        public static void UpdateSubmission(tblSubmission submission)
        {
            var result = from r in db.tblSubmissions where r.submission_ID == submission.submission_ID select r;
            result.FirstOrDefault().submission_Status = submission.submission_Status;

            db.SaveChanges();
        }

        public static void DeleteSubmission(tblSubmission submission)
        {
            var result = from r in db.tblSubmissions where r.submission_ID == submission.submission_ID select r;
            db.tblSubmissions.Remove(result.FirstOrDefault());
            db.SaveChanges();
        }
    }
}