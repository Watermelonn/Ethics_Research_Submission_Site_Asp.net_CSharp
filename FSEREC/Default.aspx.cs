﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.EntityDataSource;
using Microsoft.AspNet;
using System.Data.Entity;

namespace FSEREC
{
    public partial class Default : Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            var connection = ConfigurationManager.ConnectionStrings["NewerDatabase"];
            using (SqlConnection Sqlconnect = new SqlConnection(connection.ConnectionString))
            {
                DateTime submissionDate;
                DateTime meetingDate;

                Sqlconnect.Open();
                SqlCommand cmd = new SqlCommand("SELECT MAX(deadline_Date) FROM tblMeetingDates");

                cmd.CommandType = CommandType.Text;
                cmd.Connection = Sqlconnect;

                try { 
                submissionDate = ((DateTime)cmd.ExecuteScalar());

                lblDeadline.Text = submissionDate.ToString("dddd d MMMM yyyy");


                cmd.CommandText = "SELECT MAX(meeting_Date) FROM tblMeetingDates";

                meetingDate = ((DateTime)cmd.ExecuteScalar());
                lblMeetingDate.Text = meetingDate.ToString("dddd d MMMM yyyy");

                }
                catch
                {

                }

                Sqlconnect.Close();
            }


        }
    }
}