﻿<%@ Page Title="Login - FSE REC Submissions" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FSERec.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Stylesheet/Login.css" rel="stylesheet" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1 class="title">Login</h1>
    <div class="line"></div>
    <div id="left">
        <h2>Existing Users</h2>
        <p>For University of Chester Students/ Staff, please log in with your ID number.</p>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="Loginbtn">
                    <asp:Label ID="Emaillbl" runat="server" Text="Username/ Email" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
                    <asp:TextBox ID="Emailtxt" runat="server" Height="28px" Font-Names="Lato" Font-Size="26px" CssClass="textBoxes"></asp:TextBox>

                    <asp:Label ID="Passwordlbl" runat="server" Text="Password" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
                    <asp:TextBox ID="Passwordtxt" runat="server" Height="28px" TextMode="Password" Font-Names="Lato" Font-Size="26px" CssClass="textBoxes"></asp:TextBox>
                    <asp:Literal ID="Literal2" runat="server"></asp:Literal>

                    <asp:Label ID="lblRemember" runat="server" Text="Remember Me"></asp:Label>
                    <asp:CheckBox ID="cbxRemember" runat="server" />

                    <asp:Button ID="Loginbtn" runat="server" Text="Login" BackColor="White" BorderColor="#3C3C3C" Font-Names="Lato" Font-Size="26px" Height="50px" Width="200px" BorderWidth="1px" CssClass="buttons" OnClick="Loginbtn_Click"/>
                    <asp:Literal ID="Loginlit" runat="server"></asp:Literal>
        </asp:Panel>

    </div>

    <div id="right">

        <h2>Register</h2>

        <asp:Label ID="Label1" runat="server" Text="Firstname" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="FirstRegBox" runat="server" Height="28px" Font-Names="Lato" Font-Size="26px" CssClass="textBoxes" MaxLength="50"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="register" ErrorMessage="Your first name cannot be blank. Please enter your first name." ControlToValidate="FirstRegBox" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
            runat="server" Display="dynamic"
            ControlToValidate="FirstRegBox"
            ValidationExpression="^([\S\s]{0,50})$"
            ErrorMessage="Please enter no more than 50 characters for your Firstname" Font-Names="Lato" Font-Size="26px" CssClass="validation" ValidationGroup="register" ForeColor="Red">

        </asp:RegularExpressionValidator>
        <asp:Literal ID="Literal3" runat="server"></asp:Literal>



        <asp:Label ID="Label2" runat="server" Text="Surname" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="LastRegBox" runat="server" Height="28px" Font-Names="Lato" Font-Size="26px" CssClass="textBoxes" MaxLength="50"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" ValidationGroup="register" ErrorMessage="Your surname cannot be blank. Please enter your surname." ControlToValidate="LastRegBox" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator4"
            runat="server" Display="dynamic"
            ControlToValidate="LastRegBox"
            ValidationExpression="^([\S\s]{0,50})$"
            ErrorMessage="Please enter a maximum of 50 characters for your Surname." Font-Names="Lato" Font-Size="26px" CssClass="validation" ValidationGroup="register" ForeColor="Red">
        </asp:RegularExpressionValidator>
        <asp:Literal ID="Literal4" runat="server"></asp:Literal>



        <asp:Label ID="Label3" runat="server" Text="Email Address" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="EmailRegBox" runat="server" Height="28px" Font-Names="Lato" Font-Size="26px" TextMode="Email" CssClass="textBoxes"></asp:TextBox>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Font-Names="Lato" Font-Size="26px" ValidationGroup="register" CssClass="validation" ForeColor="Red" ErrorMessage="Your email is in an incorrect format. Please double check it and reenter it in a correct format." ControlToValidate="EmailRegBox" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Font-Names="Lato" Font-Size="26px" CssClass="validation" ValidationGroup="register" ForeColor="Red" ErrorMessage="Your email cannot be blank. Please enter your email address." ControlToValidate="EmailRegBox" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:Literal ID="Literal5" runat="server"></asp:Literal>



        <asp:Label ID="Label4" runat="server" Text="Password" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="PassRegBox" runat="server" Height="28px" Font-Names="Lato" Font-Size="26px" TextMode="Password" CssClass="textBoxes"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Font-Names="Lato" Font-Size="26px" ValidationGroup="register" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="The password cannot be blank. Please enter a password." ControlToValidate="PassRegBox" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Font-Names="Lato" Font-Size="26px" ValidationGroup="register" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="Passwords must be more than 8 characters, contain one number and one uppercase letter." ControlToValidate="PassRegBox" Display="Dynamic" ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30}$"></asp:RegularExpressionValidator>
        <asp:Literal ID="PassRegLit" runat="server"></asp:Literal>




        <asp:Label ID="Label5" runat="server" Text="Confirm Password" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="PassRegConfBox" runat="server" Height="28px" Font-Names="Lato" Font-Size="26px" TextMode="Password" CssClass="textBoxes"></asp:TextBox>
        <asp:CompareValidator ID="CompareValidator1" runat="server" Font-Names="Lato" Font-Size="26px" ValidationGroup="register" CssClass="validation" ForeColor="Red" ErrorMessage="The passwords do not match. Please double check your spelling and try again." ControlToCompare="PassRegBox" ControlToValidate="PassRegConfBox" Display="Dynamic"></asp:CompareValidator>
        <asp:Literal ID="Literal7" runat="server"></asp:Literal>




        <asp:Label ID="Label6" runat="server" Text="Organisation" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="RegOrg" runat="server" Height="28px" Font-Names="Lato" Font-Size="26px" CssClass="textBoxes"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Font-Names="Lato" ValidationGroup="register" Font-Size="26px" CssClass="validation" ForeColor="Red" ErrorMessage="The organisation name cannot be blank. Please enter the name of the organisation you work/ study for." Display="dynamic" ControlToValidate="RegOrg" SetFocusOnError="True"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator5"
            runat="server" Display="dynamic"
            ControlToValidate="LastRegBox"
            ValidationExpression="^([\S\s]{0,50})$"
            ErrorMessage="Please enter maxium 50 characters for Surname" ValidationGroup="register" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red">
        </asp:RegularExpressionValidator>


        <asp:Button ID="RegBut" runat="server" Text="Register" OnClick="RegBtn_Click" BackColor="White" BorderColor="#3C3C3C" Font-Names="Lato" Font-Size="26px" Height="50px" Width="200px" BorderWidth="1px" CssClass="buttons" ValidationGroup="register" />

        <asp:Literal ID="RegLit" runat="server"></asp:Literal>

    </div>

</asp:Content>
