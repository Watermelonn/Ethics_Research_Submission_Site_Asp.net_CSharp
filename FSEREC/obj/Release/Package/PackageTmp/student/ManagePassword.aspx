﻿<%@ Page Title="Manage Password" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManagePassword.aspx.cs" Inherits="FSEREC.Account.ManagePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Stylesheet/ManagePassword.css" rel="stylesheet" />

    <style type="text/css">
        h2 {
            padding: 0;
            margin: 0;
            margin-bottom: 20px
        }

    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1 class="title">Manage Account</h1>
    <div class="line"></div>

    <div id="left">
        <h2>Change Password</h2>
        <asp:Label ID="currentpasslbl" runat="server" Text="Current Password" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtcurrentpass" runat="server" CssClass="textBoxes" Width="300px" Height="28px" TextMode="Password" Font-Size="26px" Font-Names="Lato"></asp:TextBox>
        <asp:RequiredFieldValidator ID="currentpassvalid" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" ValidationGroup="ChangePassword" runat="server" ErrorMessage="Please Enter Your Current Password" ControlToValidate="txtcurrentpass"></asp:RequiredFieldValidator>

        <asp:Label ID="newpasslbl" runat="server" Text="New Password" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtnewpass" runat="server" Width="300px" CssClass="textBoxes" Height="28px" Font-Size="26px" Font-Names="Lato" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="newpassvalid" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" ValidationGroup="ChangePassword" runat="server" ControlToValidate="txtnewpass" ErrorMessage="Please Enter New Password"></asp:RequiredFieldValidator>

        <asp:Label ID="confpasslbl" runat="server" Text="Confirm password" Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>
        <asp:TextBox ID="txtconfpass" Width="300px" runat="server" TextMode="Password" CssClass="textBoxes" Font-Size="26px" Font-Names="Lato" Height="28px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="confpassvalid" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" ControlToValidate="txtconfpass" ErrorMessage="Please Confirm Your Password" ValidationGroup="ChangePassword"></asp:RequiredFieldValidator>
        <asp:CompareValidator ID="passwordcompvalid" runat="server" ControlToCompare="txtnewpass" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" ValidationGroup="ChangePassword" ControlToValidate="txtconfpass" ErrorMessage="The new passwords you have entered do not match. Please check your spelling and try again."></asp:CompareValidator>

        <asp:Button ID="btnupdate" CssClass="buttons" runat="server" BackColor="White" BorderColor="#3C3C3C" Font-Names="Lato" Font-Size="26px" Height="50px" ValidationGroup="ChangePassword" BorderWidth="1px" Text="Update Password" OnClick="btnupdate_Click" />
        <asp:Literal ID="ltrPassword" runat="server"></asp:Literal>

    </div>

    <div id="right">
        <h2>Change Email</h2>

        <asp:Label ID="lblEmail" runat="server" Font-Names="Lato" Font-Size="26px" CssClass="labels" Text="New Email Address"></asp:Label>
        <asp:TextBox ID="txtEmail" CausesValidation="false" CssClass="textBoxes" Height="28px" runat="server" Width="300px" Font-Size="26px" Font-Names="Lato" TextMode="Email"></asp:TextBox>
        <asp:RequiredFieldValidator ID="valReqEmail" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" ValidationGroup="NewEmail" runat="server" ErrorMessage="Please enter your new email address." ControlToValidate="txtEmail"></asp:RequiredFieldValidator>

        <asp:Label ID="lblConfirmEmail" runat="server" Font-Names="Lato" Font-Size="26px" CssClass="labels" Text="Confirm Email Address"></asp:Label>
        <asp:TextBox ID="txtConfirmEmail" CausesValidation="false" CssClass="textBoxes" Height="28px" runat="server" Width="300px" Font-Size="26px" Font-Names="Lato" TextMode="Email"></asp:TextBox>

        <asp:CompareValidator ID="valCompareEmail" runat="server" ControlToCompare="txtConfirmEmail" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" ValidationGroup="NewEmail" ControlToValidate="txtEmail" ErrorMessage="The Email addresses you have entered do not match please check your spelling and try again."></asp:CompareValidator>
        <asp:Button ID="btnemailupdate" CssClass="buttons" ValidationGroup="NewEmail" runat="server" BackColor="White" BorderColor="#3C3C3C" Font-Names="Lato" Font-Size="26px" Height="50px" Width="200px" BorderWidth="1px" Text="Update Email" OnClick="btnemailupdate_Click" />

        <asp:Literal ID="ltrEmail" runat="server"></asp:Literal>
    </div>
</asp:Content>
