﻿<%@ Page Title="New Submission - FSE REC Submissions" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="newSubmission.aspx.cs" Inherits="FSEREC.student.newSubmission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/Stylesheet/newSubmission.css" rel="stylesheet" />


    <script type="text/javascript">
        function ValidateCheckBox(sender, args) {
            if (document.getElementById("<%=cbxConfirm.ClientID %>").checked == true) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">



    <h1 class="title">New Submission</h1>
    <div class="line"></div>

    <p>
        Submitting an application is easy peasy! All you need to do is enter the title of your submission, upload the three documents needed to complete your application (the application form, references and brief CV) along with any supporting documents. The three documents must be in either a word or PDF file format and any supporting documents must be contained in a zip file.
    </p>

    <div class="submissionForm">

        <asp:Label ID="lblTitle" runat="server" Text="Submission Title" CssClass="labels" Font-Names="Lato" Font-Size="26px"></asp:Label>
        <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Height="28px" Font-Names="Lato" Font-Size="26px" Width="500px" MaxLength="50"></asp:TextBox>
        <asp:RequiredFieldValidator ID="valReqTitle" ValidationGroup="submission" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="Please enter a title for your application." ControlToValidate="txtTitle"></asp:RequiredFieldValidator>


        <asp:Label ID="lblForm" runat="server" Text="Application Form" CssClass="labels" Font-Names="Lato" Font-Size="26px"></asp:Label>
        <asp:FileUpload ID="uplForm" runat="server" CssClass="uploads" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" />
        <asp:RequiredFieldValidator ID="valReqForm" ValidationGroup="submission" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" Display="Dynamic" runat="server" ErrorMessage="This document is required to complete the application. Please upload your application form." ControlToValidate="uplForm"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="valFormatForm" ValidationGroup="submission" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" Display="Dynamic" ErrorMessage="The application form must be uploaded in either a PDF or word file." ControlToValidate="uplForm" ValidationExpression="^.+(.doc|.docx|.pdf|.docm)$"></asp:RegularExpressionValidator>

        <asp:Label ID="lblReferences" runat="server" Text="List of References" CssClass="labels" Font-Names="Lato" Font-Size="26px"></asp:Label>
        <asp:FileUpload ID="uplReferences" runat="server" CssClass="uploads" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" />
        <asp:RequiredFieldValidator ID="valReqReferences" ValidationGroup="submission" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="This document is required to complete the application. Please upload your references." ControlToValidate="uplReferences"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="valFormatReferences" ValidationGroup="submission" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="The list of references must be uploaded in either a PDF or word file." ControlToValidate="uplReferences" ValidationExpression="^.+(.doc|.docx|.pdf|.docm)$"></asp:RegularExpressionValidator>



        <asp:Label ID="lblBrief" runat="server" Text="Brief C.V. for main researcher" CssClass="labels" Font-Names="Lato" Font-Size="26px"></asp:Label>
        <asp:FileUpload ID="uplBrief" runat="server" CssClass="uploads" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" />
        <asp:RequiredFieldValidator ID="valReqBrief" ValidationGroup="submission" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="This document is required to complete the application. Please upload your brief." ControlToValidate="uplBrief"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="valFormatBrief" ValidationGroup="submission" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="The brief must be uploaded in either a PDF or word file." ControlToValidate="uplBrief" ValidationExpression="^.+(.doc|.docx|.pdf|.docm)$"></asp:RegularExpressionValidator>

        <asp:Label ID="lblSupport" runat="server" Text="Any other supporting documents" CssClass="labels" Font-Names="Lato" Font-Size="26px"></asp:Label>
        <asp:FileUpload ID="uplSupport" runat="server" CssClass="uploads" Font-Names="Lato" Font-Size="26px" BackColor="White" BorderColor="#7c7c7c" BorderWidth="1px" BorderStyle="Solid" />
        <asp:RegularExpressionValidator ID="valFormatSupport" ValidationGroup="submission" Display="Dynamic" Font-Names="Lato" Font-Size="26px" CssClass="validation" ForeColor="Red" runat="server" ErrorMessage="Any other supporting files must be uploaded in a zip file." ControlToValidate="uplSupport" ValidationExpression="^.+(.zip|.ZIP)$"></asp:RegularExpressionValidator>


        <asp:Label ID="lblConfirm" runat="server" Text="I confirm that I have uploaded all files and all are correct to the best of my knowledge." Font-Names="Lato" Font-Size="26px" CssClass="labels"></asp:Label>

        <asp:CheckBox ID="cbxConfirm" runat="server" CssClass="checkboxes" />
        <asp:CustomValidator ID="valCbxRequired" runat="server" Font-Names="Lato" Font-Size="26px" ForeColor="Red" Display="Dynamic" CssClass="validation" ErrorMessage="Please tick the checkbox to confirm that you have uploaded all files." ClientValidationFunction="ValidateCheckBox" ValidationGroup="submission"></asp:CustomValidator>



        <asp:Button ID="btnSubmit" ValidationGroup="submission" runat="server" CssClass="buttons" Text="Submit" BackColor="White" BorderColor="#7c7c7c" Font-Size="26px" Font-Names="Lato" Height="50px" Width="200px" BorderWidth="1px" OnClick="btnSubmit_Click" />
        <asp:Literal ID="SubLit" runat="server"></asp:Literal>


    </div>


</asp:Content>
