﻿<%@ Page Title="Admin Page - FSE REC Submissions" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="admin.aspx.cs" Inherits="FSEREC.admin.admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="/Stylesheet/admin.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .hiddencol {
            display: none;
        }

        .lblSearch {
            padding-right: 8px;
        }

        .txtSearch {
            margin-bottom: 20px;
            margin-right: 8px;
            padding: 5px;
        }

        .lblResults {
            padding-right: 8px;
        }

        .results {
            display: block;
            margin-bottom: 20px;
        }


        h2 {
            padding: 0;
            margin-bottom: 20px;
        }

        .calendarMeetingDateWrapper {
            margin-right: 30px;
            float: left;
            width: 300px;
        }

        .calendarDeadlineDateWrapper {
            display: inline-block;
        }

        .labels {
            margin-bottom: 20px;
            display: block;
        }

        .buttons {
            cursor: pointer;
            padding: 5px;
        }

        .controlsWrapper {
            display: block;
            margin-top:60px;
            display: block;
        }

        .dates {
            margin-top: 10px;
            display: block;
        }

        .delete {
            cursor: pointer;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1 class="title">Admin Tools</h1>
    <div class="line"></div>
    <h2 class="heading">User Accounts</h2>

    <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
        <asp:Label ID="lblSearch" CssClass="lblSearch" runat="server" Text="Search Users:" Font-Names="Lato" Font-Size="24px"></asp:Label>
        <asp:TextBox ID="txtSearch" CssClass="txtSearch" runat="server" Width="400px" placeholder="Search" Font-Size="24px" Font-Names="Lato" BorderStyle="Solid" BorderWidth="1px" BackColor="White"></asp:TextBox>
        <asp:Button ID="btnSearch" runat="server" CssClass="buttons" Text="Search" Font-Names="Lato" Font-Size="24px" BackColor="White" BorderWidth="1px" BorderStyle="Solid" BorderColor="#7c7c7c" OnClick="btnSearch_Click" />
    </asp:Panel>

    <div class="results">
        <asp:Label ID="lblResults" CssClass="lblResults" runat="server" Visible="false" Text="" Font-Names="Lato" Font-Size="28px"></asp:Label>
        <asp:LinkButton ID="btnResults" Font-Underline="false" ForeColor="IndianRed" Visible="false" runat="server" Font-Names="Lato" Font-Size="28px" OnClick="btnResults_Click">Clear</asp:LinkButton>
    </div>



    <asp:GridView ID="dgvAdmin" OnDataBound="dgvAdmin_DataBound" OnCellContentClick="dgvAdmin_CellContentClick" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" CssClass="AdminGridView" AllowPaging="True" CellPadding="5" CellSpacing="5" Font-Names="Lato" Font-Size="22px" AllowSorting="True" PageSize="20">
        <Columns>

            <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-CssClass="hiddencol" HeaderStyle-CssClass="hiddencol" ReadOnly="True" SortExpression="Id">
                <HeaderStyle CssClass="hiddencol"></HeaderStyle>

                <ItemStyle CssClass="hiddencol"></ItemStyle>
            </asp:BoundField>
            
            <asp:TemplateField>
                <ItemTemplate>
                <asp:Button ID="btnDelete" CssClass="delete" runat="server" Text="Delete" Font-Names="Lato" Font-Size="24px" OnClick="btnDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this user? All submissions, comments and uploaded made by them will also be deleted.')" />
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:BoundField DataField="Forename" HeaderText="Forename" SortExpression="Forename" />
            <asp:BoundField DataField="Surname" HeaderText="Surname" SortExpression="Surname" />
            <asp:BoundField DataField="Institution" HeaderText="Institution" SortExpression="Institution" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
            <asp:TemplateField HeaderText="Role">
                <ItemTemplate>
                    <asp:DropDownList ID="drpRoles" AutoPostBack="true" OnSelectedIndexChanged="drpRoles_SelectedIndexChanged" runat="server" Font-Names="Lato" Font-Size="22px">
                        <asp:ListItem Text="User" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Committee" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Admin" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:NewerDatabase %>" SelectCommand="SELECT AspNetUsers.Id, AspNetUsers.Forename, AspNetUsers.Surname, AspNetUsers.Institution, AspNetUsers.UserName, AspNetRoles.Name, AspNetUsers.Email FROM AspNetUsers INNER JOIN AspNetUserRoles ON AspNetUsers.Id = AspNetUserRoles.UserId INNER JOIN AspNetRoles ON AspNetUserRoles.RoleId = AspNetRoles.Id"></asp:SqlDataSource>


    <h2 class="heading">Meeting Dates</h2>
    <div class="dateSelectorWrapper">

        <div class="calendarMeetingDateWrapper">
            <asp:Label ID="lblMeetingDate" runat="server" Text="Next Committee Date:" Font-Names="Lato" Font-Size="24px" CssClass="labels"></asp:Label>
            <asp:Calendar ID="calendarMeetingDate" runat="server" OnSelectionChanged="calendarMeetingDate_SelectionChanged" FirstDayOfWeek="Monday" Font-Names="Lato">
                <DayStyle CssClass="calendarDay" />
                <SelectorStyle CssClass="calendarSelected" BackColor="#333333" ForeColor="White" />
                <TitleStyle CssClass="calendarTitle" ForeColor="White" />
                <TodayDayStyle CssClass="calendarToday" Font-Names="Lato" />
            </asp:Calendar>
            <asp:Label ID="lblSelectedDate" CssClass="dates" runat="server" Font-Size="24px" Font-Names="Lato"></asp:Label>
        </div>
        <div class="calendarDeadlineDateWrapper">

            <asp:Label ID="lblSubmissionDate" runat="server" Text="Submission Deadline Date:" Font-Names="Lato" Font-Size="24px" CssClass="labels"></asp:Label>
            <asp:Calendar ID="calendarDeadlineDate" runat="server" OnSelectionChanged="calendarDeadlineDate_SelectionChanged" FirstDayOfWeek="Monday" Font-Names="Lato">
                <DayStyle CssClass="calendarDay" />
                <SelectorStyle CssClass="calendarSelected" BackColor="#333333" ForeColor="White" />
                <TitleStyle CssClass="calendarTitle" ForeColor="White" />
                <TodayDayStyle CssClass="calendarToday" Font-Names="Lato" />
            </asp:Calendar>

            <asp:Label ID="lblDeadlineDate" CssClass="dates" runat="server" Font-Size="24px" Font-Names="Lato"></asp:Label>
        </div>

        
    </div>
<div class="controlsWrapper">
            <asp:Button ID="btnConfirm" CssClass="buttons confirm" Font-Names="Lato" Font-Size="24px" BackColor="White" BorderWidth="1px" BorderStyle="Solid" BorderColor="#7c7c7c" runat="server" Text="Confirm Dates" OnClick="btnConfirm_Click" />
        </div>
</asp:Content>
