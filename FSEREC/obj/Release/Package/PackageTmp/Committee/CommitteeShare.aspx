﻿<%@ Page Title="Committee Share - FSE REC Submissions" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CommitteeShare.aspx.cs" Inherits="FSEREC.Committee.CommitteeShare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>

        .commentItem {
            display: block;
        }

        .comment {
            margin-top: 10px;
            margin-bottom: 20px;
        }

        .commentContainer {
            background-color: #FFFFFF;
            border: solid 1px #cecece;
            padding: 10px;
        }

        .lineBreak {
            height: 1px;
            background-color: #d0d0d0;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .miniLineBreak {
            height: 1px;
            background-color: #d0d0d0;
            display: block;
            margin-top: 10px;
        }

        .time {
            float: right;
        }

        .reply {
            margin-bottom: 10px;
            margin-right: 10px;
        }

            .reply :last-child {
                margin-bottom: 0px;
            }

        h2 {
            margin-bottom: 0;
            margin-top: 10px;
        }

        .newComment {
            display: block;
            margin-top: 10px;
        }

        .upload, .attach {
            padding-right: 10px;
        }

        .upload {
            display: block;
        }

        .gridView {
            margin-top: 20px;
        }

        .add {
            margin-right: 10px;
        }

        .validation {
            margin-top: 8px;
            margin-bottom: 8px;
            display: block;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1 class="title">Committee Share</h1>
    <div class="line"></div>


    <asp:TextBox ID="txtNewComment" ValidationGroup="newComment" runat="server" Width="70%" Height="70px" placeholder="Leave a comment" TextMode="MultiLine" MaxLength="200" Font-Size="24px" Font-Names="Lato" CssClass="newComment"></asp:TextBox>
   
    <asp:LinkButton ID="btnAttach" OnClick="btnAttach_Click" CssClass="attach" runat="server" Font-Underline="False" Font-Size="24px" ForeColor="#505050" Font-Names="Lato">Attach file</asp:LinkButton>

    <asp:FileUpload ID="uplNewComment" runat="server" CssClass="upload" Font-Names="Lato" Font-Size="24px" Visible="false" />
 <asp:RegularExpressionValidator ID="val200LengthNewComment" Display="Dynamic" CssClass="validation" runat="server" ErrorMessage="Please enter a comment less than 200 characters." ControlToValidate="txtNewComment" ValidationExpression="^[\s\S]{0,200}$" ValidationGroup="newComment"></asp:RegularExpressionValidator>
    <asp:RequiredFieldValidator ID="valReqNewComment" ValidationGroup="newComment" runat="server" ErrorMessage="Please enter a comment to add." ControlToValidate="txtNewComment" Display="Dynamic" Font-Names="Lato" Font-Size="24px" CssClass="validation" ForeColor="Red"></asp:RequiredFieldValidator>

    <asp:LinkButton ID="btnNewComment" OnClick="btnNewComment_Click" ValidationGroup="newComment" CssClass="newCommentButton" runat="server" Font-Underline="False" Font-Size="24px" ForeColor="#FFCD5C5C" Font-Names="Lato">Add Comment</asp:LinkButton>





    <div class="lineBreak"></div>

    <asp:GridView runat="server" ShowHeader="False" CssClass="gridView" AutoGenerateColumns="False" DataSourceID="dgvCommitteeSource" BorderStyle="None" ID="dgvCommittee" BorderWidth="0px" CellSpacing="5" Font-Names="Lato" Font-Size="24px" GridLines="None" Width="100%" OnDataBound="dgvCommittee_DataBound" AllowPaging="True" PageSize="100">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <div class="commentContainer">
                        <asp:Label ID="lblCommitteeID" runat="server" Visible="false" Text='<%# Eval("committee_ID") %>' Font-Names="Lato"></asp:Label>
                        <asp:Label ID="lblUserID" runat="server" Text='<%# Eval("user_ID") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblSortKey" Visible="false" runat="server" Text='<%# Eval("sortKey") %>' Font-Names="Lato"></asp:Label>
                        <asp:Label ID="lblFileLocation" runat="server" Text='<%# Eval("file_Location") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFileSize" runat="server" Text='<%# Eval("file_Size") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("file_Name") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFileType" runat="server" Text='<%# Eval("file_Type") %>' Visible="false"></asp:Label>

                        <asp:Label ID="lblName" CssClass="name" runat="server" Text='<%# Eval("Forename") + " " + Eval("Surname") %>' Font-Bold="True" Font-Names="Lato"></asp:Label>
                        <asp:Label ID="lblTime" CssClass="time" runat="server" Text='<%# Eval("committee_Time") %>' ForeColor="#505050" Font-Size="22px" Font-Names="Lato"></asp:Label>
                        <asp:Label ID="lblComment" CssClass="comment commentItem" runat="server" Text='<%# Eval("committee_Comment") %>' Font-Names="Lato"></asp:Label>

                        <asp:Label ID="lblFile" CssClass="filename" runat="server" Text='<%# "Attached file: " + Eval("file_Name") %>' Font-Size="22px"></asp:Label>
                        <asp:LinkButton ID="btnDownload" CssClass="download" OnClick="btnDownload_Click" runat="server" Font-Underline="False" ForeColor="#FFCD5C5C" Font-Size="22px" Font-Names="Lato">Download</asp:LinkButton>
                        <div class="miniLineBreak"></div>
                        <asp:LinkButton ID="btnReply" OnClick="btnReply_Click" CssClass="reply" runat="server" Font-Underline="False" ForeColor="#FFCD5C5C" Font-Names="Lato">Reply </asp:LinkButton>
                        <asp:LinkButton ID="btnDelete" OnClick="btnDelete_Click" CssClass="delete" runat="server" Font-Underline="False" ForeColor="#505050" Font-Names="Lato" Visible="false">Delete</asp:LinkButton>
                       
                        <asp:TextBox ID="txtComment" ValidationGroup="reply" Visible="false" CssClass="textbox commentItem" runat="server" Font-Names="Lato" Font-Size="22px" Width="80%" Height="70px" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="val200LengthNewComment" Display="Dynamic" runat="server" ErrorMessage="Please enter a comment less than 200 characters." ControlToValidate="txtComment" ValidationExpression="^[\s\S]{0,200}$" ValidationGroup="newComment" CssClass="validation"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="valReqReply" ValidationGroup="reply" runat="server" ErrorMessage="Please enter a comment to reply." ControlToValidate="txtComment" Display="Dynamic" Font-Names="Lato" Font-Size="24px" CssClass="validation" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:LinkButton ID="btnComment" Font-Size="22px" Visible="false" ValidationGroup="reply" CssClass="add" runat="server" Font-Underline="False" ForeColor="#FFCD5C5C" Font-Names="Lato" OnClick="btnComment_Click">Add</asp:LinkButton> 
                        <asp:LinkButton ID="btnCancel" Font-Size="22px" OnClick="btnCancel_Click" Visible="false" CssClass="cancel" runat="server" Font-Underline="False" ForeColor="#505050" Font-Names="Lato">Cancel</asp:LinkButton>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerSettings Position="TopAndBottom" />
    </asp:GridView>



    <asp:SqlDataSource ID="dgvCommitteeSource" runat="server" ConnectionString="<%$ ConnectionStrings:NewerDatabase %>" SelectCommand="SELECT tblCommitteeShare.committee_ID, tblCommitteeShare.committee_Comment, tblCommitteeShare.committee_Time, tblCommitteeShare.file_ID AS Expr1, tblCommitteeShare.user_ID, tblCommitteeShare.sortKey, AspNetUsers.Id, AspNetUsers.Surname, AspNetUsers.Forename, AspNetUsers.Email, tblFile.* FROM AspNetUsers INNER JOIN tblCommitteeShare ON AspNetUsers.Id = tblCommitteeShare.user_ID LEFT OUTER JOIN tblFile ON tblCommitteeShare.file_ID = tblFile.file_ID ORDER BY tblCommitteeShare.sortKey DESC"></asp:SqlDataSource>

</asp:Content>
