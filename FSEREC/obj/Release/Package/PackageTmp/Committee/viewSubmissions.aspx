﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="viewSubmissions.aspx.cs" Inherits="FSEREC.Committee.viewSubmissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .repeaterList {
            background-color: #FFFFFF;
            border: solid 1px #cecece;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .link {
            text-decoration: none;
            color: black;
        }

        .title, .username, .submitDate, .status {
            display: block;
        }

        .line {
            margin-bottom: 50px;
        }


        .searchContainer {
            width: 600px;
            display: inline-block;
            margin-left: 50px;
        }

        .status {
            display: inline-block;
        }

        .dropDown {
            margin-bottom: 10px;
            margin-right: 20px;
        }

        .repeaterArea {
            margin: 10px 10px 10px 10px;
        }

        .submitDate {
            display: block;
        }

       .close {
           float: right;
           display: inline-block;
           
       }


    .link:visited {
        text-decoration: none;
        color: black;
    }

    .link:hover {
        color: black;
    }

    .link:focus {
        color: black;
    }

    .link:hover, a:active {
        color: black;
    }

    .noSubmissions {
         margin-top: 30px;
    }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="lblTitle" runat="server" Text="User Submissions" Font-Size="50px" Font-Names="Lato-Regular"></asp:Label>
    <div class="line"></div>

<asp:Label ID="lblNoSubmissions" Visible="false" Font-Names="Lato" Font-Size="28px" CssClass="noSubmissions" runat="server" Text="No submissions to display"></asp:Label>
    
    <asp:Label ID="lblFilter" runat="server" Text="Filter By:" Font-Names="Lato" Font-Size="22px"></asp:Label>
    <asp:DropDownList ID="drpFilter" CssClass="dropDown" runat="server" Font-Names="Lato" Font-Size="22px" Width="300px" BackColor="White" AutoPostBack="True" OnSelectedIndexChanged="drpFilter_SelectedIndexChanged">
        <asp:ListItem>Show All</asp:ListItem>
        <asp:ListItem>Open</asp:ListItem>
        <asp:ListItem>Closed</asp:ListItem>
    </asp:DropDownList>

    <asp:Label ID="lblSort" runat="server" Text="Sort By:" Font-Names="Lato" Font-Size="22px"></asp:Label>
    <asp:DropDownList ID="drpSort" CssClass="dropDown" runat="server" Font-Names="Lato" Font-Size="22px" Width="350px" BackColor="White" AutoPostBack="True" OnSelectedIndexChanged="drpSort_SelectedIndexChanged">
        <asp:ListItem>Submit Date: (Newest to Oldest)</asp:ListItem>
        <asp:ListItem>Submit Date: (Oldest to Newest)</asp:ListItem>
    </asp:DropDownList>

    <asp:Repeater ID="repSubmissions" runat="server" DataSourceID="repDataSource" OnItemCommand="repSubmissions_ItemCommand">

        <HeaderTemplate>
            <div class="repeaterContainer">
        </HeaderTemplate>
        <ItemTemplate>

            <div class="repeaterList">

                <a class="link" href="/submissionDetail?submission=<%# Eval("submission_Id") %>">
                    <div class="repeaterArea">
                        
                            <asp:Label ID="lblSubmissionID" runat="server" Visible="false" Text='<%# Eval("submission_ID") %>'></asp:Label>
                            <asp:Label ID="lblHeading" CssClass="title" runat="server" Font-Size="40px" Font-Names="Lato" Text='<%#Eval("submission_Title")%>'></asp:Label>
                            <asp:Label ID="lblName" CssClass="username" runat="server" Font-Size="26px" Font-Names="Lato" Text='<%#Eval("Forename") + " " + Eval("Surname")%>'></asp:Label>
                            <asp:Label ID="lblUserName" CssClass="username" runat="server" Font-Size="26px" Width="600px" Font-Names="Lato" Text='<%#Eval("Email")%>'></asp:Label>
                            <asp:Label ID="lblSubmitDate" CssClass="submitDate" runat="server" Width="450px" Font-Size="26px" Font-Names="Lato" Text='<%#Eval("submission_SubmitDate")%>'></asp:Label>
                            <asp:Label ID="lblStatus" CssClass="status" runat="server" Font-Size="26px" Font-Names="Lato" ForeColor='<%#(Eval("submission_Status").Equals("Open")) ? System.Drawing.Color.LimeGreen : System.Drawing.Color.Red %>' Text='<%#Eval("submission_Status")%>'></asp:Label>
                            
                        
                    </div>
                </a>

            </div>

        </ItemTemplate>
        <FooterTemplate></div></FooterTemplate>

    </asp:Repeater>


    <asp:SqlDataSource ID="repDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NewerDatabase %>" SelectCommand="SELECT tblSubmission.*, AspNetUsers.Id, AspNetUsers.Forename, AspNetUsers.Surname, AspNetUsers.Email FROM tblSubmission INNER JOIN tblUser_Submission ON tblSubmission.submission_ID = tblUser_Submission.submission_ID INNER JOIN AspNetUsers ON tblUser_Submission.Id = AspNetUsers.Id"></asp:SqlDataSource>


</asp:Content>
