﻿<%@ Page Title="Success! - FSE REC Submissions" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="signOut.aspx.cs" Inherits="FSEREC.signOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .line {
            margin-bottom: 20px;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1 class="title">Success!</h1>
    <div class="line"></div>
    <asp:Label ID="lblSuccess" runat="server" Text="" Font-Size="26px" Font-Names="Lato" CssClass="label"></asp:Label>

</asp:Content>
