This website was developed for the on campus ethical research commitee during the work based learning module that I undertook. It features a multi user role system including the ability for students to submit applications and committee members to view, comment and close applications.
Built in ASP.net C#.

![](/FSEREC/Images/shot-mini-20180911-19981-fdqw0s.jpeg)